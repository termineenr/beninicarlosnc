insert into clientefornitore(piva,nome_cf,indirizzo_cf,tel_cf,cliente,fornitore) values ('fornitore_2','nome_5','indirizzo_5','02231132',false,true)

insert into contatto(nome_cnt,tel_cnt,mail_cnt,clientefornitore,area_cnt) values ('contatto_4','0424343232','contatto2@gmail.com',null,'spedizioni');

insert into macchina(codice_mac,modello,nserie,anno_costr,anno_acq,cespite,term_garan,term_leasing,dealer) values ('0045442','sony','265767236443','2011-09-22','2011-12-22','dfjls','2020','2023','');

insert into manutenzione(macchina,tipo_man,descr_man,data_man,problema_man,operatore,tempi_man,costi_man,parti_sostituite,riferimento,note_man,periodo,eseguita) values ('0042442','straordinaria','descrizione','2015-11-11','','mauro','45 min','100.00','cinghia','rottura','','non periodico','false');

insert into maschera(codice_mas,materiale_mas,tempi_attrezz) values ('22253452','alluminio','30 min');

insert into materiale(fornitore,lega,prezzokg,dimensioni,costo_mat) values ('fornitore_2','titanio','122.15','42x655','335');

insert into offerta (clientefornitore,data_off) values ('cliente_3','2016-11-11');

insert into pezzo (codice_p,codp_cliente,desc_p,materiale,tipo_p,maschera,lavorazione,programma,revisione_p) values ('pezzo_2','03747758','descrizionepezzo',null,'fuso','22253452','lavorazione1','programma2','1');

insert into ordine (id_cliente,clientefornitore,offerta,data_ordine,desc_ordine,sconto_ordine,data_distr,data_cons,prezzo_tot) values ('20743r94','cliente_1','1','2014-12-12','descrizioneordine','100','2014-12-14','2014-12-16','1000');

insert into ordinepezzo(ordine,codice_p,revisione_p,quantità_op,lotto,prezzo_op,attrezzatura,costo_attrezz,soffiaggio) values ('1','pezzo_1','0','2000','a12122015','20000','','200','no');

insert into utensile (nome_ut,tipo,diametro,lunghezza,taglienti,marca,note) values ('utensile_4','tipo4','200','800','tagliente_4','marca_3','note_4');

insert into setpezzo (codice_p,revisione_p,utensile,macchina,posizione) values ('pezzo_1','0','utensile_2','0042442','45');

insert into listino (codice_p,revisione_p,offerta,richiesta,prezzo_p,sconto_list,quantità,attrezzatura,costo_attrezz,soffiaggio) values ('pezzo_1','0','2','richiesta1','11220','2220','120','attrezzatura_2','300','soffiaggio_2');

insert into dfl(codice_p,revisione_p,ordine,tempi_lavorazione,npezzi,data_cons,data_stimata,problemi,controllo,imballaggio) values ('pezzo_1','0','1','200','1000','2016-12-12','2016-12-08','','controllo_1','imballaggio_1');

insert into utensilefornitore (fornitore,utensile,prezzo_ut) values ('fornitore_1','utensile_1','100');

insert into rubrica(macchina,contatto) values ('','');