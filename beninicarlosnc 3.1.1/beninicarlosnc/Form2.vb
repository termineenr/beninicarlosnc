﻿Imports MySql.Data.MySqlClient
Public Class Form2
    Dim cn As New MySqlConnection
    Dim dbcomm As MySqlCommand
    Dim dbread As MySqlDataReader
    Dim sql As String
    Dim key As String


    Public Sub connessione()

        Dim connectionString = Form1.connectionString
        Dim connectionString2 = Form1.connectionString2

        cn = New MySqlConnection(connectionString)
        Try
            cn.Open()
        Catch ex As Exception
            MsgBox("Errore Di Connessione")

        End Try


    End Sub
    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Form1.par_cnt = "CLIENTEFORNITORE" Then


            connessione()
            Try
                Dim z = "Select * from CLIENTEFORNITORE where piva ='" + Form1.par2 + "'"
                dbcomm = New MySqlCommand(z, cn)
                dbread = dbcomm.ExecuteReader()
                While dbread.Read()
                    Label1.Text = dbread(1)
                End While
                dbread.Close()
            Catch ex As Exception

            End Try
            Try
                sql = "select * from CONTATTO where clientefornitore ='" + Form1.par2 + "'"
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                DataGridView1.Rows.Clear()
                While dbread.Read()
                    DataGridView1.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(4))
                End While
                dbread.Close()
            Catch ex As Exception

            End Try
        Else
            Label1.Text = Form1.par2
            connessione()
            Try
                dbcomm = New MySqlCommand("Select * from RUBRICA where macchina ='" + Form1.par2 + "'", cn)
                dbread = dbcomm.ExecuteReader()
                While dbread.Read()
                    Label1.Text = dbread(0)
                End While
                dbread.Close()
            Catch ex As Exception

            End Try
            Try
                sql = "select * from CONTATTO JOIN RUBRICA on contatto = tel_cnt where macchina ='" + Form1.par2 + "'"
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                DataGridView1.Rows.Clear()
                While dbread.Read()
                    DataGridView1.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(4))
                End While
                dbread.Close()
            Catch ex As Exception

            End Try
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Form1.par_cnt = "CLIENTEFORNITORE" Then

            sql = "insert into CONTATTO(nome_cnt,tel_cnt,mail_cnt,clientefornitore,area_cnt) values('" + TextBox5.Text + "','" + TextBox6.Text + "','" + TextBox7.Text + "','" + Form1.par2 + "','" + TextBox1.Text + "')"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("Esiste Già Un Elemento Con Lo Stesso Identificatore")
            End Try
            Try
                sql = "select * from CONTATTO where clientefornitore ='" + Form1.par2 + "'"
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                DataGridView1.Rows.Clear()
                While dbread.Read()
                    DataGridView1.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(4))
                End While
                dbread.Close()
            Catch ex As Exception

            End Try
            Form1.loadContatto()
            TextBox5.Text = ""
            TextBox6.Text = ""
            TextBox7.Text = ""
            TextBox1.Text = ""
        Else
            sql = "insert into CONTATTO(nome_cnt,tel_cnt,mail_cnt,clientefornitore,area_cnt) values('" + TextBox5.Text + "','" + TextBox6.Text + "','" + TextBox7.Text + "','" + "null" + "','" + TextBox1.Text + "')"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("Esiste Già Un Elemento Con Lo Stesso Identificatore")
            End Try

            sql = "insert into RUBRICA(contatto,macchina) values('" + TextBox6.Text + " ','" + Form1.par2 + "')"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("Esiste Già Un Elemento Con Lo Stesso Identificatore")
            End Try

            Try
                sql = "select * from CONTATTO JOIN RUBRICA ON contatto = tel_cnt where macchina ='" + Form1.par2 + "'"
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                DataGridView1.Rows.Clear()
                While dbread.Read()
                    DataGridView1.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(4))
                End While
                dbread.Close()
            Catch ex As Exception

            End Try
            Form1.loadContatto()
            TextBox5.Text = ""
            TextBox6.Text = ""
            TextBox7.Text = ""
            TextBox1.Text = ""
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If Form1.conf_elimina() = 6 Then
            sql = "delete from CONTATTO where tel_cnt='" + DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(1).FormattedValue().ToString() + "'"
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
            dbread.Close()
            If Form1.par_cnt = "CLIENTEFORNITORE" Then
                sql = "select * from CONTATTO where clientefornitore ='" + Form1.par2 + "'"
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                DataGridView1.Rows.Clear()
                Try
                    While dbread.Read()
                        DataGridView1.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(4))
                    End While
                    dbread.Close()

                Catch ex As Exception

                End Try
            Else
                Try
                    sql = "select * from CONTATTO JOIN RUBRICA ON contatto = tel_cnt where macchina ='" + Form1.par2 + "'"
                    dbcomm = New MySqlCommand(sql, cn)
                    dbread = dbcomm.ExecuteReader()
                    DataGridView1.Rows.Clear()
                    While dbread.Read()
                        DataGridView1.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(4))
                    End While
                    dbread.Close()
                Catch ex As Exception

                End Try

            End If
        Form1.loadContatto()
        End If
        TextBox5.Text = ""
        TextBox6.Text = ""
        TextBox7.Text = ""
        TextBox1.Text = ""
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Form1.par_cnt = "CLIENTEFORNITORE" Then


            If Form1.conf_modifica() = 6 Then
                Dim sql = "select tel_cnt from CONTATTO where tel_cnt='" + TextBox6.Text + "'"

                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                Dim c = ""
                While dbread.Read()
                    c = dbread(0)
                End While
                dbread.Close()
                sql = "update CONTATTO set tel_cnt='" + TextBox6.Text + "', nome_cnt ='" + TextBox5.Text + "',mail_cnt ='" + TextBox7.Text + "', area_cnt ='" + TextBox1.Text + "' where tel_cnt ='" + key + "'"
                Try
                    dbcomm = New MySqlCommand(sql, cn)
                    dbread = dbcomm.ExecuteReader()
                    dbread.Close()
                Catch ex As Exception
                    MsgBox("Il numero esiste già per un altro contatto")
                End Try
                Try
                    sql = "select * from CONTATTO where clientefornitore = '" + Form1.par2 + "'"
                    dbcomm = New MySqlCommand(sql, cn)
                    dbread = dbcomm.ExecuteReader()
                Catch ex As Exception
                    MsgBox("Errore")
                End Try

                DataGridView1.Rows.Clear()
                Try
                    While dbread.Read()
                        DataGridView1.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(4))
                    End While
                    dbread.Close()

                Catch ex As Exception

                End Try
                Form1.loadContatto()
            End If
            TextBox5.Text = ""
            TextBox6.Text = ""
            TextBox7.Text = ""
            TextBox1.Text = ""

        Else

            If Form1.conf_modifica() = 6 Then
                Dim sql = "select contatto from RUBRICA where contatto='" + TextBox6.Text + "'"

                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                Dim c = ""
                While dbread.Read()
                    c = dbread(0)
                End While
                dbread.Close()
                sql = "update CONTATTO set tel_cnt='" + TextBox6.Text + "', nome_cnt ='" + TextBox5.Text + "',mail_cnt ='" + TextBox7.Text + "', area_cnt ='" + TextBox1.Text + "' where tel_cnt ='" + key + "'"
                Try
                    dbcomm = New MySqlCommand(sql, cn)
                    dbread = dbcomm.ExecuteReader()
                    dbread.Close()
                Catch ex As Exception
                    MsgBox("Il numero esiste già per un altro contatto")
                End Try
                Try
                    sql = "select * from CONTATTO JOIN RUBRICA ON contatto = tel_cnt where macchina ='" + Form1.par2 + "'"
                    dbcomm = New MySqlCommand(sql, cn)
                    dbread = dbcomm.ExecuteReader()
                Catch ex As Exception
                    MsgBox("Errore")
                End Try

                DataGridView1.Rows.Clear()
                Try
                    While dbread.Read()
                        DataGridView1.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(4))
                    End While
                    dbread.Close()

                Catch ex As Exception

                End Try
                Form1.loadContatto()
            End If
            TextBox5.Text = ""
            TextBox6.Text = ""
            TextBox7.Text = ""
            TextBox1.Text = ""


        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        key = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(1).FormattedValue().ToString()
        TextBox5.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        TextBox6.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(1).FormattedValue().ToString()
        TextBox7.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(2).FormattedValue().ToString()
        TextBox1.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(3).FormattedValue().ToString()
    End Sub
End Class