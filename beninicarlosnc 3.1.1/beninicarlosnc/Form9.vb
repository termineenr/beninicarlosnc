﻿
Imports MySql.Data.MySqlClient

Public Class Form9

    Dim cn As New MySqlConnection
    Dim dbcomm As MySqlCommand
    Dim dbread As MySqlDataReader
    Dim elem As Integer
    Dim sql As String

    Public Sub connessione()

        Dim connectionString = Form1.connectionString
        Dim connectionString2 = Form1.connectionString2
        cn = New MySqlConnection(connectionString)
            Try
                cn.Open()
            Catch ex As Exception
                MsgBox("Errore Di Connessione")

            End Try


        End Sub


        Public Sub query(sql As String)
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
            Catch ex As Exception
                MsgBox("Esiste Già Un Elemento Con Lo Stesso Identificatore")
            End Try

        End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Form9_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Label2.Text = Form1.pgtord
        connessione()
        query("select codice_p from ORDINEPEZZO where ordine =" + Form1.pgtord)

        While dbread.Read()
            ComboBox1.Items.Add(dbread(0))
        End While

            dbread.Close()
            cn.Close()
        End Sub

        Private Sub ComboBox1_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedValueChanged
            connessione()
        query("select revisione_p from ORDINEPEZZO where ordine =" + Form1.pgtord + " and codice_p ='" + ComboBox1.SelectedItem.ToString() + "'")

        While dbread.Read()
            ComboBox2.Items.Add(dbread(0))
        End While

        dbread.Close()
        cn.Close()

    End Sub

    Private Sub ComboBox2_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBox2.SelectedValueChanged
        connessione()
        Try
            dbcomm = New MySqlCommand("select * from DFL where ordine =" + Form1.pgtord + " and codice_p ='" + ComboBox1.SelectedItem.ToString() + "' and revisione_p='" + ComboBox2.SelectedItem.ToString() + "'", cn)
            dbread = dbcomm.ExecuteReader()

        Catch ex As Exception
            MsgBox("Errore")
        End Try
        elem = 0
        Try
            While dbread.Read()
                TextBox1.Text = dbread(5)
                TextBox2.Text = dbread(4)
                DateTimePicker1.Text = dbread(7)
                DateTimePicker2.Text = dbread(6)
                TextBox3.Text = dbread(8)
                TextBox4.Text = dbread(9)
                TextBox5.Text = dbread(10)
                elem = 1
            End While
        Catch ex As Exception
        End Try



        dbread.Close()
        cn.Close()

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        connessione()
        Static dfl = 1000

        If elem = 0 Then
            sql = "insert into DFL(id_dfl,codice_p,revisione_p,ordine,tempi_lavorazione,npezzi,data_cons,data_stimata,problemi,controllo,imballaggio) values('" + Form1.pgtord + dfl.ToString() + "','" + ComboBox1.SelectedItem.ToString() + "','" + ComboBox2.SelectedItem.ToString() + "'," + Form1.pgtord + "," + TextBox2.Text + ",'" + TextBox1.Text + "','" + DateTimePicker2.Value.ToString("yyyy-MM-dd") + "','" + DateTimePicker1.Value.ToString("yyyy-MM-dd") + "','" + TextBox3.Text + "','" + TextBox4.Text + "','" + TextBox5.Text + "')"
            dfl = dfl + 1
            query(sql)
            cn.Close()
            MsgBox("Dfl inserito")
            elem = 1
        ElseIf elem = 1 Then
            If Form1.conf_modifica() = 6 Then
                sql = "update DFL set tempi_lavorazione='" + TextBox2.Text + "', npezzi='" + TextBox1.Text + "', data_cons='" + DateTimePicker2.Value.ToString("yyyy-MM-dd") + "', data_stimata='" + DateTimePicker1.Value.ToString("yyyy-MM-dd") + "', problemi='" + TextBox3.Text + "', controllo='" + TextBox4.Text + "', imballaggio='" + TextBox5.Text + "' where codice_p='" + ComboBox1.SelectedItem.ToString() + "' and revisione_p='" + ComboBox2.SelectedItem.ToString() + "' and ordine='" + Form1.pgtord + "'"
                query(sql)
                cn.Close()
                MsgBox("Dfl aggiornato")
            End If
        End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        connessione()

        If elem = 0 Then
            MsgBox("Nessun dfl presente")

        ElseIf elem = 1 Then

            If Form1.conf_elimina() = 6 Then

                sql = "delete from DFL where codice_p='" + ComboBox1.SelectedItem.ToString() + "' and revisione_p='" + ComboBox2.SelectedItem.ToString() + "' and ordine='" + Form1.pgtord + "'"
                query(sql)
                MsgBox("Dfl eliminato")

                TextBox1.Text = ""
                TextBox2.Text = ""
                TextBox3.Text = ""
                TextBox4.Text = ""
                TextBox5.Text = ""
                DateTimePicker1.Value.ToLocalTime()
                DateTimePicker2.Value.ToLocalTime()
                elem = 0
            End If

        End If
    End Sub
End Class

