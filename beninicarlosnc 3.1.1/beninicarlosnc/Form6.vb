﻿Imports MySql.Data.MySqlClient



Public Class Form6
    Dim cn As New MySqlConnection
    Dim dbcomm As MySqlCommand
    Dim dbread As MySqlDataReader
    Dim macchina As String
    Dim tipo_man As String
    Public Function conf_elimina() As Integer
        Return MsgBox("Sicuro di voler eliminare l'elemento selezionato?", 4)
    End Function

    Public Function conf_modifica() As Integer
        Return MsgBox("Sicuro di voler modificare l'elemento selezionato?", 4)
    End Function
    Public Sub connessione()

        Dim connectionString = Form1.connectionString
        Dim connectionString2 = Form1.connectionString2
        cn = New MySqlConnection(connectionString)
        Try
            cn.Open()
        Catch ex As Exception
            MsgBox("Errore Di Connessione")

        End Try


    End Sub


    Public Sub query(sql As String)
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
        Catch ex As Exception
            MsgBox("Esiste Già Un Elemento Con Lo Stesso Identificatore")
        End Try

    End Sub

    Private Sub Form6_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        macchina = Form1.par6
        Label1.Text = "Macchina: " + macchina
        periodo.Items.Add("Nessuno")
        periodo.Items.Add("Giornaliero")
        periodo.Items.Add("Mensile")
        periodo.Items.Add("Semestrale")
        periodo.Items.Add("Annuale")
        loadManutenzione()

    End Sub
    Public Sub loadManutenzione()
        connessione()
        DataGridView1.Rows.Clear()
        Dim f As DataGridViewCheckBoxCell

        query("select * from MANUTENZIONE where macchina='" + macchina + "'")

        Dim i = 0
        Try
            While dbread.Read()

                DataGridView1.Rows.Add(dbread(3), dbread(4), dbread(5), dbread(6), dbread(7), dbread(8), dbread(9), dbread(10), dbread(11), dbread(2), dbread(12), dbread(0))
                f = DataGridView1.Rows(i).Cells(12)


                If dbread(13) = "1" Then

                    f.Value = True
                Else
                    f.Value = False
                End If
                i = i + 1
            End While
            dbread.Close()
        Catch ex As Exception
            MsgBox("ERRORE")
        End Try


        cn.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        connessione()
        Dim fatto As String

        If RadioButton1.Checked Then
            tipo_man = "Ordinaria"
        Else
            tipo_man = "Straordinaria"
        End If
        If RadioButton3.Checked Then
            fatto = "0"
        Else
            fatto = "1"
        End If
        Dim sql = "INSERT INTO MANUTENZIONE(macchina, tipo_man, descr_man, data_man, problema_man, operatore, tempi_man, costi_man, parti_sostituite, riferimento, note_man, periodo,eseguita) VALUES ('" + macchina + "','" + tipo_man + "','" + descr_man.Text + "','" + data_man.Value.ToString("yyyy-MM-dd") + "','" + prob_man.Text + "','" + operatore.Text + "','" + tempi_man.Text + "','" + costi_man.Text + "','" + parti_sostituite.Text + "','" + riferimento.Text + "','" + note_man.Text + "','" + periodo.Text + "','" + fatto + "')"
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
            dbread.Close()
        Catch ex As Exception
            MsgBox("ERRORE!!!!!")
        End Try
        cn.Close()
        loadManutenzione()

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        connessione()
        Dim fatto As String
        Dim id_man = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(11).FormattedValue().ToString()
        If conf_modifica() = 6 Then
            If RadioButton1.Checked Then
                tipo_man = "Ordinaria"
            Else
                tipo_man = "Straordinaria"
            End If
            If RadioButton3.Checked Then
                fatto = "0"
            Else
                fatto = "1"
            End If
            Dim sql = "update MANUTENZIONE set tipo_man='" + tipo_man + "',descr_man='" + descr_man.Text + "',data_man='" + data_man.Value.ToString("yyyy-MM-dd") + "',problema_man='" + prob_man.Text + "',operatore='" + operatore.Text + "',tempi_man='" + tempi_man.Text + "',costi_man=" + costi_man.Text + ",parti_sostituite='" + parti_sostituite.Text + "',riferimento='" + riferimento.Text + "',note_man='" + note_man.Text + "',periodo='" + periodo.SelectedItem.ToString() + "', eseguita='" + fatto + "'  where id_man=" + id_man + ""
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE")
            End Try

            cn.Close()
        End If

        DataGridView1.Rows.Clear()
        loadManutenzione()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        connessione()

        If conf_elimina() = 6 Then
            Dim id_man = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(11).FormattedValue().ToString()
            Dim sql = "delete from MANUTENZIONE where id_man=" + id_man + ""
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE!!!!!")
            End Try
            cn.Close()
            DataGridView1.Rows.Clear()
            loadManutenzione()

        End If
    End Sub

    Private Sub DataGridView1_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        descr_man.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        data_man.Value = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(1).FormattedValue().ToString()
        prob_man.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(2).FormattedValue().ToString()
        operatore.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(3).FormattedValue().ToString()
        tempi_man.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(4).FormattedValue().ToString()
        costi_man.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(5).FormattedValue().ToString()
        parti_sostituite.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(6).FormattedValue().ToString()
        riferimento.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(7).FormattedValue().ToString()
        note_man.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(8).FormattedValue().ToString()
        tipo_man = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(9).FormattedValue().ToString()
        periodo.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(10).FormattedValue().ToString()
        Dim fatto As String
        fatto = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(12).FormattedValue().ToString()
        If tipo_man = "Straordinaria" Then
            RadioButton2.Checked = True
            RadioButton1.Checked = False
        ElseIf tipo_man = "Ordinaria"
            RadioButton1.Checked = True
            RadioButton2.Checked = False
        End If
        If fatto = "0" Then
            RadioButton3.Checked = True
            RadioButton4.Checked = False
        ElseIf fatto = "1" Then
            RadioButton4.Checked = True
            RadioButton3.Checked = False
        End If
    End Sub
End Class