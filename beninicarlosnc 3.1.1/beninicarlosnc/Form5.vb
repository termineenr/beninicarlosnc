﻿Imports MySql.Data.MySqlClient

Public Class Form5
    Dim cn As New MySqlConnection
    Dim dbcomm As MySqlCommand
    Dim dbread As MySqlDataReader

    Public Sub connessione()

        Dim connectionString = Form1.connectionString
        Dim connectionString2 = Form1.connectionString2
        cn = New MySqlConnection(connectionString)
        Try
            cn.Open()
        Catch ex As Exception
            MsgBox("Errore Di Connessione")

        End Try


    End Sub


    Public Sub query(sql As String)
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
        Catch ex As Exception
            MsgBox("Esiste Già Un Elemento Con Lo Stesso Identificatore")
        End Try

    End Sub
    Private Sub Form5_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        RadioButton1.Checked = True
        loadMaschera()
        loadMateriale()

    End Sub
    Public Sub loadMaschera()
        maschera.Items.Clear()

        connessione()
        query("select distinct codice_mas from MASCHERA")
        Try
            While dbread.Read()
                maschera.Items.Add(dbread(0))
            End While
        Catch ex As Exception
            MsgBox("ERRORE")
        End Try

        dbread.Close()
        cn.Close()
    End Sub

    Public Sub loadMateriale()
        materiale.Items.Clear()

        connessione()
        query("select distinct * from MATERIALE")
        Try
            While dbread.Read()
                materiale.Items.Add(dbread(0))
            End While
        Catch ex As Exception
            MsgBox("ERRORE")
        End Try
        dbread.Close()
        cn.Close()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click


        If RadioButton1.Checked = True And materiale.Text = "" Then
                MsgBox("Per i pezzi pieni va inserito il materiale")
            ElseIf RadioButton2.Checked = True And materiale.Text <> "" Then
                MsgBox("Per i pezzi fusi non va inserito il materiale")
            Else

                Dim tipo_p As String = "Errore"
                If RadioButton1.Checked = True Then
                    tipo_p = "Pieno"
                ElseIf RadioButton2.Checked = True Then
                    tipo_p = "Fuso"
                End If
                Dim mat As String
                Try
                    mat = materiale.SelectedItem.ToString()
                Catch ex As Exception
                    mat = 0
                End Try
                connessione()
                Dim sql = "INSERT INTO PEZZO (codice_p, codp_cliente, desc_p, materiale, tipo_p, maschera, lavorazione, programma, revisione_p) VALUES ('" + codice.Text + "','" + codice_cli.Text + "', '" + desc_p.Text + "'," + mat.ToString + ",'" + tipo_p + "','" + maschera.Text + "','" + lavorazione.Text + "','" + programma.Text + "','" + revisione_p.Text + "')"
                Try
                    dbcomm = New MySqlCommand(sql, cn)
                    dbread = dbcomm.ExecuteReader()
                    dbread.Close()
                    MsgBox("Pezzo aggiunto")
                    Form1.loadPezzo()
                Catch ex As Exception
                    MsgBox("ERRORE!!!!!")
                End Try
                Try
                    Form15.ComboBox1.Items.Add(codice.Text)
                Catch ex As Exception

                End Try
                Try
                    Form14.ComboBox1.Items.Add(codice.Text)
                Catch ex As Exception

                End Try
            End If

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Form7.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Form8.Show()
    End Sub
End Class