﻿Imports MySql.Data.MySqlClient
Imports System.String

Public Class Form1
    Dim cn As New MySqlConnection
    Dim dbcomm As MySqlCommand
    Dim dbread As MySqlDataReader
    Public AME As String
    Public par2 As String
    Public par4 As String
    Public par6 As String
    Public par12_pez As String
    Public par12_rev As String
    Public par12_mac As String
    Public pgtord As String
    Dim keyclifor As String
    Dim keymac As String
    Dim keyute As String
    Public connectionString As String
    Public connectionString2 As String
    Public idofferta As String
    Public par3 As String
    Public par17 As String
    Public par_cnt As String

    Public Function conf_elimina() As Integer
        Return MsgBox("Sicuro di voler eliminare l'elemento selezionato?", 4)
    End Function

    Public Function conf_modifica() As Integer
        Return MsgBox("Sicuro di voler modificare l'elemento selezionato?", 4)
    End Function

    Public Sub connessione()

        connectionString = "Data Source=localhost;user id=root;database=beninicarlosnc;password="
        connectionString2 = "Data Source=sql7.freemysqlhosting.net;user id=sql7109704;database=sql7109704;password=sgmvh9YBv4"

        cn = New MySqlConnection(connectionString)
        Try
            cn.Open()
        Catch ex As Exception
            MsgBox("Errore Di Connessione")

        End Try


    End Sub


    Public Sub query(sql As String)
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
        Catch ex As Exception
            MsgBox("Esiste Già Un Elemento Con Lo Stesso Identificatore")
        End Try

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        ComboBox4.Items.Add("Piva")
        ComboBox4.Items.Add("Nome")
        ComboBox4.Items.Add("Indirizzo")
        ComboBox4.Items.Add("Telefono")


        ComboBox1.Items.Add("Nome")
        ComboBox1.Items.Add("Telefono")
        ComboBox1.Items.Add("E-Mail")
        ComboBox1.Items.Add("Cliente/Fornitore")
        ComboBox1.Items.Add("Area Competenza")

        ComboBox9.Items.Add("Ordine")
        ComboBox9.Items.Add("Ordine Cliente")
        ComboBox9.Items.Add("Cliente")
        ComboBox9.Items.Add("Data Ordine")
        ComboBox9.Items.Add("Data Distribuzione")
        ComboBox9.Items.Add("Data Consegna")

        ComboBox5.Items.Add("Codice")
        ComboBox5.Items.Add("Modello")
        ComboBox5.Items.Add("N° serie")
        ComboBox5.Items.Add("Data costruzione")
        ComboBox5.Items.Add("Data acquisto")
        ComboBox5.Items.Add("CESPITE")
        ComboBox5.Items.Add("Dealer")

        ComboBox6.Items.Add("Nome")
        ComboBox6.Items.Add("Tipo")
        ComboBox6.Items.Add("Diametro")
        ComboBox6.Items.Add("Lunhezza")
        ComboBox6.Items.Add("Taglienti")
        ComboBox6.Items.Add("Marca")

        ComboBox7.Items.Add("Codice Nostro")
        ComboBox7.Items.Add("Codice Cliente")

        ComboBox11.Items.Add("ID")
        ComboBox11.Items.Add("Lega")
        ComboBox11.Items.Add("Fornitore")

        ComboBox8.Items.Add("Offerta")
        ComboBox8.Items.Add("Pezzo")
        ComboBox8.Items.Add("Richiesta")

        ComboBox2.Items.Add("Offerta")
        ComboBox2.Items.Add("Cliente")
        ComboBox2.Items.Add("Data")

        loadClienteFornitore()
        loadContatto()
        loadOrdine()
        loadMacchina()
        loadUtensili()
        loadPezzo()
        loadSet()
        loadMateriale()
        loadListino()
        loadOfferte()
    End Sub
    Public Sub loadOfferte()

        connessione()

        query("select  * from OFFERTA")
        DataGridView3.Rows.Clear()
        Try
            While dbread.Read()
                DataGridView3.Rows.Add(dbread(0), dbread(1), dbread(2))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()

    End Sub
    Public Sub loadListino()
        DataGridView4.Rows.Clear()
        connessione()
        query("select * from LISTINO")
        While dbread.Read()
            DataGridView4.Rows.Add(dbread(2), dbread(0), dbread(1), dbread(6), dbread(4), dbread(5), dbread(3), dbread(9), dbread(7), dbread(8))
        End While
        dbread.Close()
        cn.Close()
    End Sub
    Public Sub loadMateriale()
        connessione()
        DataGridView8.Rows.Clear()
        ComboBox12.Items.Clear()



        Dim Sql = "Select * from CLIENTEFORNITORE WHERE fornitore=True"
        dbcomm = New MySqlCommand(Sql, cn)
        dbread = dbcomm.ExecuteReader()
        Dim i = 0
        While dbread.Read()
            ComboBox12.Items.Insert(i, dbread(0))
            i = i + 1
        End While
        dbread.Close()
        cn.Close()

        connessione()

        query("select  * from MATERIALE")
        DataGridView8.Rows.Clear()
        Try
            While dbread.Read()
                DataGridView8.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), dbread(4), dbread(5))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()
    End Sub
    Public Sub loadSet()
        connessione()
        ComboBox10.Items.Clear()
        ComboBox3.Items.Clear()
        DataGridView10.Rows.Clear()
        DataGridView11.Rows.Clear()
        TextBox26.Clear()

        query("select distinct codice_p from SETPEZZO")
        Try
            While dbread.Read()
                ComboBox10.Items.Add(dbread(0))

            End While
        Catch ex As Exception
            MsgBox("ERRORE")
        End Try

        dbread.Close()
        cn.Close()

        ComboBox3.Text = ""
        ComboBox10.Text = ""
    End Sub

    Public Sub loadOrdine()
        connessione()
        query("select  * from ORDINE")
        DataGridView9.Rows.Clear()
        Try
            While dbread.Read()
                DataGridView9.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), dbread(4), dbread(5), dbread(7), dbread(8), dbread(9), dbread(9), dbread(6))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()
    End Sub

    Public Sub loadClienteFornitore()
        connessione()
        query("select * from CLIENTEFORNITORE")
        DataGridView1.Rows.Clear()
        Dim cliente As String
        Dim fornitore As String

        Try
            While dbread.Read()
                cliente = "No"
                fornitore = "No"
                If dbread(4) = "1" Then
                    cliente = "Sì"
                End If
                If dbread(5) = "1" Then
                    fornitore = "Sì"
                End If
                DataGridView1.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), cliente, fornitore)
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()

    End Sub

    Public Sub loadContatto()
        connessione()
        query("select * from CONTATTO")
        DataGridView2.Rows.Clear()
        Try
            While dbread.Read()
                DataGridView2.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), dbread(4))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()
    End Sub

    Public Sub loadMacchina()
        connessione()
        query("select * from MACCHINA")
        DataGridView5.Rows.Clear()
        Try
            While dbread.Read()
                DataGridView5.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), dbread(4), dbread(5), dbread(6), dbread(7), dbread(8))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()

    End Sub

    Public Sub loadUtensili()
        connessione()
        ComboBox13.Items.Clear()
        Dim Sql = "Select * from CLIENTEFORNITORE WHERE fornitore=True"
        Try
            dbcomm = New MySqlCommand(Sql, cn)
            dbread = dbcomm.ExecuteReader()
            Dim i = 0
            While dbread.Read()
                ComboBox13.Items.Insert(i, dbread(0))
                i = i + 1
            End While
        Catch ex As Exception
        End Try
        dbread.Close()
        cn.Close()


        connessione()
        query("select * from UTENSILE left join UTENSILEFORNITORE ON nome_ut=utensile")
        DataGridView6.Rows.Clear()
        Try
            While dbread.Read()
                DataGridView6.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), dbread(4), dbread(5), dbread(6), dbread(7), dbread(9))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()
    End Sub

    Public Sub loadPezzo()
        connessione()
        query("select * from PEZZO")
        DataGridView7.Rows.Clear()
        Try
            While dbread.Read()
                DataGridView7.Rows.Add(dbread(0), dbread(1), dbread(8), dbread(2), dbread(6), dbread(7), dbread(4), dbread(3))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()
    End Sub







    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        par3 = "aggiungi"
        Form3.Show()
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs)
        AME = "Aggiungi"
        par4 = "-1"
        Form4.Show()

    End Sub

    Private Sub DataGridView1_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView1.CellMouseClick
        keyclifor = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        TextBox5.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(1).FormattedValue().ToString()
        TextBox4.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        TextBox3.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(2).FormattedValue().ToString()
        TextBox2.Text = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(3).FormattedValue().ToString()
        CheckBox1.Checked = False
        CheckBox2.Checked = False
        If DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(4).FormattedValue().ToString() = "Sì" Then
            CheckBox1.Checked = True
        End If
        If DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(5).FormattedValue().ToString() = "Sì" Then
            CheckBox2.Checked = True
        End If
    End Sub

    Private Sub Button16_Click(sender As Object, e As EventArgs) Handles Button16.Click
        connessione()

        Try
            Dim s As String

            If (ComboBox4.SelectedItem.ToString().Equals("Nome")) Then
                s = "nome_cf"
            ElseIf (ComboBox4.SelectedItem.ToString().Equals("Piva")) Then
                s = "piva"
            ElseIf (ComboBox4.SelectedItem.ToString().Equals("Telefono")) Then
                s = "tel_cf"
            ElseIf (ComboBox4.SelectedItem.ToString().Equals("Indirizzo")) Then
                s = "indirizzo_cf"
            End If



            dbcomm = New MySqlCommand("select * from CLIENTEFORNITORE where " + s + " = '" + TextBox13.Text.Trim() + "'", cn)
            dbread = dbcomm.ExecuteReader()

            DataGridView1.Rows.Clear()
            Dim cliente As String
            Dim fornitore As String
            While dbread.Read()
                cliente = "No"
                fornitore = "No"
                If dbread(4) = "1" Then
                    cliente = "Sì"
                End If
                If dbread(5) = "1" Then
                    fornitore = "Sì"
                End If
                DataGridView1.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), cliente, fornitore)
            End While
            dbread.Close()
        Catch ex As Exception

        End Try
        cn.Close()
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        connessione()

        If TextBox4.Text = "" Or TextBox5.Text = "" Then
            MsgBox("Campo Nome E Piva Obbligatori")
        ElseIf (Not CheckBox1.Checked) And (Not CheckBox2.Checked) Then
            MsgBox("Deve Essere Un Cliente e/o Un Fornitore")
        Else

            Dim sql = "insert into CLIENTEFORNITORE (piva,nome_cf,indirizzo_cf,tel_cf,cliente,fornitore) values('" + TextBox4.Text.Trim().Trim() + "','" + TextBox5.Text.Trim().Trim() + "','" + TextBox3.Text.Trim().Trim() + "','" + TextBox2.Text.Trim().Trim() + "'," + CheckBox1.Checked.ToString() + "," + CheckBox2.Checked.ToString() + ")"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
                sql = "select * from CLIENTEFORNITORE"
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
            Catch ex As Exception
                MsgBox("ERRORE!!!!!")
            End Try

            loadClienteFornitore()
        End If
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        connessione()

        If conf_modifica() = 6 Then
            Dim sql = "update CLIENTEFORNITORE set piva ='" + TextBox4.Text.Trim().Trim() + "',nome_cf ='" + TextBox5.Text.Trim().Trim() + "',indirizzo_cf ='" + TextBox3.Text.Trim().Trim() + "',tel_cf ='" + TextBox2.Text.Trim().Trim() + "',cliente=" + CheckBox1.Checked.ToString() + ",fornitore=" + CheckBox2.Checked.ToString() + " where piva='" + keyclifor + "'"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("Esiste già un altro cliente con la stessa partita iva")
            End Try

            cn.Close()
            DataGridView1.Rows.Clear()
            loadClienteFornitore()


        End If


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        connessione()

        If conf_elimina() = 6 Then
            Dim sql = "delete from CLIENTEFORNITORE where piva='" + TextBox4.Text.Trim() + "'"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE!!!!!")
            End Try
            cn.Close()
            loadClienteFornitore()

        End If

    End Sub

    Private Sub Button22_Click(sender As Object, e As EventArgs) Handles Button22.Click
        loadClienteFornitore()
    End Sub

    Private Sub DataGridView1_DoubleClick(sender As Object, e As EventArgs) Handles DataGridView1.DoubleClick
        '' Try
        ''par_cnt = "CLIENTEFORNITORE"
        ''par2 = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        ''Form2.Show()
        ''Catch ex As Exception

        ''End Try
    End Sub

    Private Sub Button31_Click(sender As Object, e As EventArgs)
        Form6.Show()

    End Sub

    Private Sub Button30_Click(sender As Object, e As EventArgs)
        AME = "Aggiungi"
        Form5.Show()

    End Sub

    Private Sub DataGridView4_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
        Form9.Show()
    End Sub

    Private Sub DataGridView7_CellContentDoubleClick(sender As Object, e As DataGridViewCellEventArgs)
        Form10.Show()
    End Sub

    Private Sub DataGridView8_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs)
        Form11.Show()

    End Sub

    Private Sub Button23_Click(sender As Object, e As EventArgs) Handles Button23.Click
        loadContatto()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        connessione()

        Try
            Dim s As String

            If (ComboBox1.SelectedItem.ToString().Equals("Nome")) Then
                s = "nome_cnt"
            ElseIf (ComboBox1.SelectedItem.ToString().Equals("Telefono")) Then
                s = "tel_cnt"
            ElseIf (ComboBox1.SelectedItem.ToString().Equals("E-Mail")) Then
                s = "mail_cnt"
            ElseIf (ComboBox1.SelectedItem.ToString().Equals("Cliente/Fornitore")) Then
                s = "clientefornitore"
            ElseIf (ComboBox1.SelectedItem.ToString().Equals("Macchina")) Then
                s = "macchina"
            ElseIf (ComboBox1.SelectedItem.ToString().Equals("Area Competenza")) Then
                s = "area_cnt"
            End If



            dbcomm = New MySqlCommand("select * from CONTATTO where " + s + " = '" + TextBox1.Text.Trim().Trim() + "'", cn)
            dbread = dbcomm.ExecuteReader()

            DataGridView2.Rows.Clear()

            While dbread.Read()
                DataGridView2.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), dbread(4))
            End While
            dbread.Close()
        Catch ex As Exception

        End Try
        cn.Close()
    End Sub

    Private Sub Button26_Click(sender As Object, e As EventArgs)


    End Sub

    Private Sub Button36_Click(sender As Object, e As EventArgs)
        loadOrdine()

    End Sub

    Private Sub Button25_Click(sender As Object, e As EventArgs) Handles Button25.Click
        loadMacchina()

    End Sub

    Private Sub Button24_Click(sender As Object, e As EventArgs) Handles Button24.Click
        loadUtensili()

    End Sub

    Private Sub Button28_Click(sender As Object, e As EventArgs) Handles Button28.Click
        loadPezzo()

    End Sub



    Private Sub Button17_Click(sender As Object, e As EventArgs) Handles Button17.Click
        connessione()

        Try
            Dim s As String
            If (ComboBox5.SelectedItem.ToString().Equals("Codice")) Then
                s = "codice_mac"
            ElseIf (ComboBox5.SelectedItem.ToString().Equals("Modello")) Then
                s = "modello"
            ElseIf (ComboBox5.SelectedItem.ToString().Equals("N° serie")) Then
                s = "nserie"
            ElseIf (ComboBox5.SelectedItem.ToString().Equals("Data costruzione")) Then
                s = "data_costr"
            ElseIf (ComboBox5.SelectedItem.ToString().Equals("Data acquisto")) Then
                s = "data_cons"
            ElseIf (ComboBox5.SelectedItem.ToString().Equals("CESPITE")) Then
                s = "cespite"
            ElseIf (ComboBox5.SelectedItem.ToString().Equals("Dealer")) Then
                s = "dealer"
            End If


            dbcomm = New MySqlCommand("select * from MACCHINA where " + s + " = '" + TextBox14.Text.Trim().Trim() + "'", cn)
            dbread = dbcomm.ExecuteReader()

            DataGridView5.Rows.Clear()

            While dbread.Read()
                DataGridView5.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), dbread(4), dbread(5), dbread(6), dbread(7), dbread(8))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()
    End Sub

    Private Sub Button18_Click(sender As Object, e As EventArgs) Handles Button18.Click



        Dim s As String
            If (ComboBox6.SelectedItem.ToString().Equals("Nome")) Then
                s = "nome_ut"
            ElseIf (ComboBox6.SelectedItem.ToString().Equals("Tipo")) Then
                s = "tipo"
            ElseIf (ComboBox6.SelectedItem.ToString().Equals("Diametro")) Then
                s = "diametro"
            ElseIf (ComboBox6.SelectedItem.ToString().Equals("Lunghezza")) Then
                s = "lunghezza"
            ElseIf (ComboBox6.SelectedItem.ToString().Equals("Taglienti")) Then
                s = "taglienti"
            ElseIf (ComboBox6.SelectedItem.ToString().Equals("Marca")) Then
                s = "marca"
            End If



            connessione()
        query("select * from UTENSILE left join UTENSILEFORNITORE ON nome_ut=utensile where " + s + " = '" + TextBox15.Text.Trim().Trim() + "'")
        DataGridView6.Rows.Clear()
            Try
                While dbread.Read()
                    DataGridView6.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), dbread(4), dbread(5), dbread(6), dbread(7), dbread(9))
                End While
                dbread.Close()
            Catch ex As Exception
            End Try
            cn.Close()
    End Sub

    Private Sub Button29_Click(sender As Object, e As EventArgs) Handles Button29.Click
        connessione()

        Try
            Dim s As String
            If (ComboBox7.SelectedItem.ToString().Equals("Codice Nostro")) Then
                s = "codice_p"
            ElseIf (ComboBox6.SelectedItem.ToString().Equals("Codice Cliente")) Then
                s = "codp_cliente"

            End If


            dbcomm = New MySqlCommand("select * from PEZZO where " + s + " = '" + TextBox23.Text.Trim().Trim() + "'", cn)
            dbread = dbcomm.ExecuteReader()

            DataGridView7.Rows.Clear()

            While dbread.Read()
                DataGridView7.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(4))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()
    End Sub

    Private Sub Button33_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub Button15_Click(sender As Object, e As EventArgs) Handles Button15.Click
        connessione()

        Dim sql = "insert into MACCHINA (codice_mac, modello, nserie, anno_costr, anno_acq, cespite, term_garan, term_leasing, dealer) values('" + TextBox11.Text.Trim().Trim() + "','" + TextBox8.Text.Trim().Trim() + "','" + TextBox9.Text.Trim().Trim() + "','" + DateTimePicker4.Value.ToString("yyyy-MM-dd") + "','" + DateTimePicker1.Value.ToString("yyyy-MM-dd") + "','" + TextBox10.Text.Trim().Trim() + "','" + DateTimePicker2.Value.ToString("yyyy-MM-dd") + "','" + DateTimePicker3.Value.ToString("yyyy-MM-dd") + "','" + TextBox12.Text.Trim().Trim() + "')"
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
            dbread.Close()
        Catch ex As Exception
            MsgBox("ERRORE!!!!!")
        End Try
        cn.Close()
        loadMacchina()

    End Sub

    Private Sub Button21_Click(sender As Object, e As EventArgs) Handles Button21.Click
        connessione()

        Dim sql = "INSERT INTO UTENSILE(nome_ut, tipo, diametro, lunghezza, taglienti, marca, note) values('" + TextBox20.Text.Trim() + "','" + TextBox17.Text.Trim() + "','" + TextBox18.Text.Trim() + "','" + TextBox16.Text.Trim().Trim() + "','" + TextBox19.Text.Trim().Trim() + "','" + TextBox22.Text.Trim().Trim() + "','" + TextBox21.Text.Trim().Trim() + "')"
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
            dbread.Close()
        Catch ex As Exception

        End Try
        cn.Close()
        Dim fornitore As String
        Dim prezzo As String
        connessione()
        Try
            fornitore = ComboBox13.SelectedItem.ToString()
        Catch ex As Exception
            fornitore = ""
        End Try

        prezzo = TextBox27.Text.Trim()
        If fornitore <> "" Then
            sql = "INSERT INTO UTENSILEFORNITORE(fornitore, utensile, prezzo_ut) values('" + fornitore + "','" + TextBox20.Text.Trim() + "','" + prezzo + "')"

            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE!!!!!")
            End Try
            cn.Close()
        End If
        loadUtensili()

    End Sub

    Private Sub Button13_Click(sender As Object, e As EventArgs) Handles Button13.Click
        connessione()

        If conf_modifica() = 6 Then
            Dim sql = "update MACCHINA set codice_mac='" + TextBox11.Text.Trim() + "', modello='" + TextBox8.Text.Trim() + "', nserie='" + TextBox9.Text.Trim() + "', anno_costr='" + DateTimePicker4.Value.ToString("yyyy-MM-dd") + "', anno_acq='" + DateTimePicker1.Value.ToString("yyyy-MM-dd") + "', cespite='" + TextBox10.Text.Trim() + "', term_garan='" + DateTimePicker2.Value.ToString("yyyy-MM-dd") + "', term_leasing='" + DateTimePicker3.Value.ToString("yyyy-MM-dd") + "', dealer='" + TextBox12.Text.Trim() + "' where codice_mac='" + keymac + "'"
            MsgBox(sql)
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("Esiste già una macchina con quel codice")
            End Try

            cn.Close()
            DataGridView1.Rows.Clear()
            loadMacchina()


        End If
    End Sub

    Private Sub DataGridView5_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView5.CellContentClick
        keymac = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        TextBox11.Text = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        TextBox8.Text = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(1).FormattedValue().ToString()
        TextBox9.Text = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(2).FormattedValue().ToString()
        DateTimePicker4.Value = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(3).FormattedValue().ToString()
        DateTimePicker1.Value = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(4).FormattedValue().ToString()
        TextBox10.Text = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(5).FormattedValue().ToString()
        DateTimePicker2.Value = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(6).FormattedValue().ToString()
        DateTimePicker3.Value = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(7).FormattedValue().ToString()
        TextBox12.Text = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(8).FormattedValue().ToString()
    End Sub

    Private Sub Button14_Click(sender As Object, e As EventArgs) Handles Button14.Click
        connessione()

        If conf_elimina() = 6 Then
            Dim sql = "delete from MACCHINA where codice_mac='" + TextBox11.Text.Trim() + "'"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE!!!!!")
            End Try
            cn.Close()
            loadMacchina()

        End If


    End Sub

    Private Sub Button19_Click(sender As Object, e As EventArgs) Handles Button19.Click
        connessione()

        If conf_modifica() = 6 Then
            Dim sql = "update UTENSILE set nome_ut='" + TextBox20.Text.Trim() + "', tipo='" + TextBox17.Text.Trim() + "', diametro='" + TextBox18.Text.Trim() + "', lunghezza='" + TextBox16.Text.Trim() + "', taglienti='" + TextBox19.Text.Trim() + "', marca='" + TextBox22.Text.Trim() + "', note='" + TextBox21.Text.Trim() + "' where nome_ut='" + keyute + "'"

            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("Esiste già un utensile con quel codice")
            End Try

            cn.Close()
            Try
                sql = "delete from UTENSILEFORNITORE where fornitore='" + DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(7).FormattedValue().ToString() + "' and utensile='" + DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(0).FormattedValue().ToString() + "'"

            Catch ex As Exception

            End Try
            Try
                connessione()
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception

            End Try

            cn.Close()

            sql = "INSERT INTO UTENSILEFORNITORE (fornitore, utensile, prezzo_ut) VALUES ('" + ComboBox13.SelectedItem.ToString() + "','" + TextBox20.Text.Trim() + "'," + TextBox27.Text.Trim() + ")"

            Try

                connessione()
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()

            Catch ex As Exception

            End Try

            cn.Close()

            DataGridView1.Rows.Clear()
            loadUtensili()
            TextBox20.Text = ""
            TextBox17.Text = ""
            TextBox18.Text = ""
            TextBox16.Text = ""
            TextBox19.Text = ""
            TextBox22.Text = ""
            TextBox21.Text = ""
            TextBox27.Text = ""
            ComboBox13.Text = ""

        End If

    End Sub

    Private Sub Button20_Click(sender As Object, e As EventArgs) Handles Button20.Click
        connessione()

        If conf_elimina() = 6 Then
            Dim sql = "DELETE FROM UTENSILEFORNITORE where utensile='" + TextBox20.Text.Trim() + "' AND fornitore='" + DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(7).FormattedValue().ToString() + "'"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE!!!!!")
            End Try
            cn.Close()
            connessione()

            Try
                sql = "delete from UTENSILEFORNITORE where fornitore='" + ComboBox13.SelectedItem.ToString() + "' and utensile='" + TextBox20.Text.Trim() + "'"
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception

            End Try
            cn.Close()
            loadUtensili()
        End If
        TextBox20.Text = ""
        TextBox17.Text = ""
        TextBox18.Text = ""
        TextBox16.Text = ""
        TextBox19.Text = ""
        TextBox22.Text = ""
        TextBox21.Text = ""
        TextBox27.Text = ""
        ComboBox13.Text = ""
    End Sub

    Private Sub DataGridView6_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView6.CellContentClick
        keyute = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        TextBox20.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        TextBox17.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(1).FormattedValue().ToString()
        TextBox18.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(2).FormattedValue().ToString()
        TextBox16.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(3).FormattedValue().ToString()
        TextBox19.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(4).FormattedValue().ToString()
        TextBox22.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(5).FormattedValue().ToString()
        TextBox21.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(6).FormattedValue().ToString()
        TextBox27.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(8).FormattedValue().ToString()
        ComboBox13.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(7).FormattedValue().ToString()
    End Sub

    Private Sub Button35_Click(sender As Object, e As EventArgs)
        Form9.Show()
    End Sub

    Private Sub ComboBox10_SelectedValueChanged(sender As Object, e As EventArgs) Handles ComboBox10.SelectedValueChanged
        connessione()
        query("select distinct revisione_p from SETPEZZO where  codice_p ='" + ComboBox10.SelectedItem.ToString() + "'")
        ComboBox3.Items.Clear()

        While dbread.Read()
            ComboBox3.Items.Add(dbread(0))
        End While

        dbread.Close()
        cn.Close()

    End Sub

    Private Sub DataGridView10_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView10.CellClick
        connessione()
        Try
            query("select utensile,posizione from SETPEZZO where codice_p = '" + ComboBox10.SelectedItem.ToString() + "' and revisione_p = '" + ComboBox3.SelectedItem.ToString() + "' and macchina = '" + DataGridView10.Rows(DataGridView10.CurrentRow.Index()).Cells(0).FormattedValue().ToString() + "'")
            DataGridView11.Rows.Clear()

            While dbread.Read()
                DataGridView11.Rows.Add(dbread(0), dbread(1))
            End While

            dbread.Close()
        Catch ex As Exception

        End Try
        cn.Close()

    End Sub

    Private Sub Button39_Click(sender As Object, e As EventArgs) Handles Button39.Click
        AME = "Aggiungi"
        Form4.Show()
    End Sub

    Private Sub Button12_Click(sender As Object, e As EventArgs) Handles Button12.Click
        AME = "Modifica"
        Try
            par6 = DataGridView9.Rows(DataGridView9.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
            Form4.Button1.Text() = AME
            Form4.Show()
        Catch ex As Exception
            MsgBox("Non hai selezionato una cella")

        End Try


    End Sub

    Private Sub Button11_Click(sender As Object, e As EventArgs) Handles Button11.Click
        If conf_elimina() = 6 Then
            connessione()
            Try
                Dim p = DataGridView9.Rows(DataGridView9.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
                query("delete from ORDINE where id_ordine = " + p)
                dbread.Close()

            Catch ex As Exception
                MsgBox("Non hai selezionato una cella")

            End Try
            Try
                Dim p = DataGridView9.Rows(DataGridView9.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
                query("delete from ORDINEPEZZO where ordine = " + p)
                dbread.Close()

            Catch ex As Exception
                MsgBox("Non hai selezionato una cella")

            End Try

            cn.Close()
            loadOrdine()

        End If
    End Sub

    Private Sub Button7_Click_1(sender As Object, e As EventArgs) Handles Button7.Click
        AME = "Aggiungi"
        Form12.Show()

    End Sub

    Private Sub Button10_Click_1(sender As Object, e As EventArgs) Handles Button10.Click
        connessione()

        Try
            Dim s As String
            If (ComboBox9.SelectedItem.ToString().Equals("Ordine")) Then
                s = "id_ordine"
            ElseIf (ComboBox9.SelectedItem.ToString().Equals("Ordine Cliente")) Then
                s = "id_cliente"
            ElseIf (ComboBox9.SelectedItem.ToString().Equals("Cliente")) Then
                s = "clientefornitore"
            ElseIf (ComboBox9.SelectedItem.ToString().Equals("Offerta")) Then
                s = "offerta"
            ElseIf (ComboBox9.SelectedItem.ToString().Equals("Data Ordine")) Then
                s = "data_ordine"
            ElseIf (ComboBox9.SelectedItem.ToString().Equals("Data Distribuzione")) Then
                s = "data_distr"
            ElseIf (ComboBox9.SelectedItem.ToString().Equals("Data Consegna")) Then
                s = "data_cons"
            End If


            dbcomm = New MySqlCommand("select * from ORDINE where " + s + " = '" + TextBox25.Text + "'", cn)
            dbread = dbcomm.ExecuteReader()

            DataGridView9.Rows.Clear()

            While dbread.Read()
                DataGridView9.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), dbread(4), dbread(5), dbread(6), dbread(7), dbread(8), dbread(9), dbread(6))
            End While
            dbread.Close()
        Catch ex As Exception
            MsgBox("ERRORE")
        End Try
        cn.Close()
    End Sub

    Private Sub Button26_Click_1(sender As Object, e As EventArgs) Handles Button26.Click
        loadOrdine()
    End Sub

    Private Sub DataGridView9_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView9.CellDoubleClick
        pgtord = DataGridView9.Rows(DataGridView9.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        Form9.Show()
    End Sub

    Private Sub Button34_Click(sender As Object, e As EventArgs) Handles Button34.Click
        pgtord = DataGridView9.Rows(DataGridView9.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        Form9.Button3.Visible = False
        Form9.Button2.Visible = False
        Form9.TextBox1.ReadOnly = True
        Form9.TextBox2.ReadOnly = True
        Form9.TextBox3.ReadOnly = True
        Form9.TextBox4.ReadOnly = True
        Form9.TextBox5.ReadOnly = True
        Form9.DateTimePicker1.Enabled = False
        Form9.DateTimePicker2.Enabled = False
        Form9.Show()
    End Sub





    Private Sub DataGridView5_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView5.CellDoubleClick
        par6 = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        Form6.Show()

    End Sub

    Private Sub ComboBox3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox3.SelectedIndexChanged
        connessione()
        Try
            query("select distinct macchina from SETPEZZO where codice_p ='" + ComboBox10.SelectedItem.ToString() + "' AND revisione_p='" + ComboBox3.SelectedItem.ToString() + "'")
            DataGridView10.Rows.Clear()
            While dbread.Read()
                DataGridView10.Rows.Add(dbread(0))
            End While
            dbread.Close()
        Catch ex As Exception

        End Try
        cn.Close()
        connessione()
        Dim materiale As String
        materiale = ""
        Try
            query("select materiale from PEZZO where codice_p ='" + ComboBox10.SelectedItem.ToString() + "' AND revisione_p='" + ComboBox3.SelectedItem.ToString() + "'")

            While dbread.Read()
                materiale = dbread(0)
            End While
            dbread.Close()
        Catch ex As Exception

        End Try
        cn.Close()
        connessione()
        Try
            query("select * from MATERIALE where id_materiale ='" + materiale + "'")

            While dbread.Read()

                TextBox26.Text = vbCrLf + "id materiale=" + dbread(0).ToString() + vbCrLf + vbCrLf + "fornitore=" + dbread(1).ToString() + vbCrLf + vbCrLf + "lega=" + dbread(2).ToString() + vbCrLf + vbCrLf + "prezzo/kg=" + dbread(3).ToString() + vbCrLf + vbCrLf + "dimensioni=" + dbread(4).ToString() + vbCrLf + vbCrLf + "costo materiale=" + dbread(5).ToString()
                TextBox26.ReadOnly = True

            End While
            dbread.Close()
        Catch ex As Exception

        End Try
        cn.Close()
    End Sub

    Private Sub Button32_Click(sender As Object, e As EventArgs) Handles Button32.Click
        AME = "Modifica"
        par12_pez = ComboBox10.SelectedItem.ToString
        par12_rev = ComboBox3.SelectedItem.ToString
        par12_mac = DataGridView10.Rows(DataGridView10.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        Form12.Show()
    End Sub

    Private Sub Button31_Click_1(sender As Object, e As EventArgs) Handles Button31.Click
        connessione()

        If conf_elimina() = 6 Then
            Dim sql = "delete from SETPEZZO where codice_p='" + ComboBox10.SelectedItem.ToString() + "' and macchina='" + DataGridView10.Rows(DataGridView10.CurrentRow.Index()).Cells(0).FormattedValue().ToString() + "' and revisione_p='" + ComboBox3.SelectedItem.ToString() + "'"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE!!!!!")
            End Try
            cn.Close()
            loadSet()
        End If
    End Sub

    Private Sub Button36_Click_1(sender As Object, e As EventArgs) Handles Button36.Click
        loadMateriale()

    End Sub

    Private Sub Button41_Click(sender As Object, e As EventArgs) Handles Button41.Click
        connessione()
        Dim sql As String = "INSERT INTO MATERIALE( fornitore, lega, prezzokg, dimensioni, costo_mat) VALUES ('" + ComboBox12.SelectedItem.ToString() + "','" + TextBox31.Text.Trim() + "'," + TextBox29.Text.Trim() + ",'" + TextBox32.Text.Trim() + "'," + TextBox24.Text.Trim() + ")"
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
            dbread.Close()
            MsgBox("Materiale inserito")

        Catch ex As Exception
            MsgBox("ERRORE!!!!!")
        End Try
        cn.Close()
        loadMateriale()

    End Sub

    Private Sub Button38_Click(sender As Object, e As EventArgs) Handles Button38.Click
        connessione()

        If conf_modifica() = 6 Then
            Dim sql = "update  MATERIALE set fornitore='" + ComboBox12.SelectedItem.ToString() + "',lega='" + TextBox31.Text.Trim() + "',prezzokg='" + TextBox29.Text.Trim() + "',dimensioni='" + TextBox32.Text.Trim() + "',costo_mat='" + TextBox24.Text.Trim() + "' where id_materiale='" + DataGridView8.Rows(DataGridView8.CurrentRow.Index()).Cells(0).FormattedValue().ToString() + "'"

            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("Esiste già un utensile con quel codice")
            End Try

            cn.Close()
            sql = ""
            Try
                sql = "update UTENSILEFORNITORE set prezzo_ut=" + TextBox27.Text.Trim() + " where fornitore='" + ComboBox13.SelectedItem.ToString() + "' and utensile='" + TextBox20.Text.Trim() + "'"

            Catch ex As Exception

            End Try
            Try
                connessione()
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception

            End Try

            cn.Close()



            DataGridView8.Rows.Clear()
            loadMateriale()
            TextBox31.Text = ""
            TextBox29.Text = ""
            TextBox32.Text = ""
            TextBox24.Text = ""
            ComboBox12.Text = ""
        End If

    End Sub

    Private Sub DataGridView8_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView8.CellClick

        TextBox31.Text = DataGridView8.Rows(DataGridView8.CurrentRow.Index()).Cells(2).FormattedValue().ToString()
        TextBox29.Text = DataGridView8.Rows(DataGridView8.CurrentRow.Index()).Cells(3).FormattedValue().ToString()
        TextBox32.Text = DataGridView8.Rows(DataGridView8.CurrentRow.Index()).Cells(4).FormattedValue().ToString()
        TextBox24.Text = DataGridView8.Rows(DataGridView8.CurrentRow.Index()).Cells(5).FormattedValue().ToString()
        ComboBox12.Text = DataGridView8.Rows(DataGridView8.CurrentRow.Index()).Cells(1).FormattedValue().ToString()
    End Sub

    Private Sub Button40_Click_1(sender As Object, e As EventArgs) Handles Button40.Click
        connessione()

        If conf_elimina() = 6 Then
            Dim sql = "delete from MATERIALE where id_materiale='" + DataGridView8.Rows(DataGridView8.CurrentRow.Index()).Cells(0).FormattedValue().ToString() + "'"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE! Selezionare una cella!")
            End Try
            cn.Close()
        End If
        loadMateriale()

    End Sub

    Private Sub Button37_Click(sender As Object, e As EventArgs) Handles Button37.Click
        connessione()

        Try
            Dim s As String
            If (ComboBox11.SelectedItem.ToString().Equals("Lega")) Then
                s = "lega"
            ElseIf (ComboBox11.SelectedItem.ToString().Equals("ID")) Then
                s = "id_materiale"
            ElseIf (ComboBox11.SelectedItem.ToString().Equals("Fornitore")) Then
                s = "fornitore"
            End If

            dbcomm = New MySqlCommand("select * from MATERIALE where " + s + " = '" + TextBox28.Text.Trim() + "'", cn)
            dbread = dbcomm.ExecuteReader()

            DataGridView8.Rows.Clear()

            While dbread.Read()
                DataGridView8.Rows.Add(dbread(0), dbread(1), dbread(2), dbread(3), dbread(4), dbread(5))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()
    End Sub

    Private Sub DataGridView7_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView7.CellDoubleClick
        Form10.pezzo = DataGridView7.Rows(DataGridView7.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        Form10.revisione = DataGridView7.Rows(DataGridView7.CurrentRow.Index()).Cells(2).FormattedValue().ToString()
        Form10.Show()

    End Sub

    Private Sub DataGridView6_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView6.CellClick
        keyute = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        TextBox20.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        TextBox17.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(1).FormattedValue().ToString()
        TextBox18.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(2).FormattedValue().ToString()
        TextBox16.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(3).FormattedValue().ToString()
        TextBox19.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(4).FormattedValue().ToString()
        TextBox22.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(5).FormattedValue().ToString()
        TextBox21.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(6).FormattedValue().ToString()
        TextBox27.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(8).FormattedValue().ToString()
        ComboBox13.Text = DataGridView6.Rows(DataGridView6.CurrentRow.Index()).Cells(7).FormattedValue().ToString()
    End Sub

    Private Sub Button42_Click(sender As Object, e As EventArgs) Handles Button42.Click
        Try


            Form11.pezzo = ComboBox10.SelectedItem.ToString()
            Form11.revisione = ComboBox3.SelectedItem.ToString()
            Form11.macchina = DataGridView10.Rows(DataGridView10.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
            Form11.Show()
        Catch ex As Exception
            MsgBox("Seleziona Pezzo, Revisione e Macchina")
        End Try
    End Sub

    Private Sub Button35_Click_1(sender As Object, e As EventArgs) Handles Button35.Click
        connessione()

        Try
            Dim s As String
            If (ComboBox8.SelectedItem.ToString().Equals("Offerta")) Then
                s = "offerta"
            ElseIf (ComboBox8.SelectedItem.ToString().Equals("Pezzo")) Then
                s = "codice_p"
            ElseIf (ComboBox8.SelectedItem.ToString().Equals("Richiesta")) Then
                s = "richiesta"

            End If


            dbcomm = New MySqlCommand("select * from LISTINO where " + s + " = '" + TextBox7.Text.Trim() + "'", cn)
            dbread = dbcomm.ExecuteReader()

            DataGridView4.Rows.Clear()

            While dbread.Read()
                DataGridView4.Rows.Add(dbread(2), dbread(0), dbread(1), dbread(6), dbread(4), dbread(5), dbread(3), dbread(9), dbread(7), dbread(8))
            End While
            dbread.Close()
        Catch ex As Exception
        End Try
        cn.Close()
    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        par3 = "modifica"
        idofferta = DataGridView3.Rows(DataGridView3.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        Form3.Show()
    End Sub

    Private Sub Button43_Click(sender As Object, e As EventArgs) Handles Button43.Click
        AME = "Visualizza"
        Try
            par6 = DataGridView9.Rows(DataGridView9.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
            Form4.Button1.Text() = AME
            Form4.Show()
        Catch ex As Exception
            MsgBox("Non hai selezionato una cella")

        End Try
    End Sub

    Private Sub Button27_Click(sender As Object, e As EventArgs) Handles Button27.Click
        loadOfferte()

    End Sub


    Private Sub Button8_Click(sender As Object, e As EventArgs) Handles Button8.Click
        idofferta = DataGridView3.Rows(DataGridView3.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        connessione()
        query("delete from LISTINO where offerta='" + idofferta + "'")
        dbread.Close()
        query("delete from OFFERTA where id_offerta='" + idofferta + "'")
        dbread.Close()
        cn.Close()
        loadOfferte()
        loadListino()

    End Sub

    Private Sub DataGridView3_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles DataGridView3.CellMouseDoubleClick
        par17 = DataGridView3.Rows(DataGridView3.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
        Form16.Show()
    End Sub

    Private Sub Button33_Click_1(sender As Object, e As EventArgs) Handles Button33.Click
        loadListino()

    End Sub

    Private Sub Button30_Click_1(sender As Object, e As EventArgs) Handles Button30.Click
        Try
            par_cnt = "RUBRICA"
            par2 = DataGridView5.Rows(DataGridView5.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
            Form2.Show()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DataGridView1_CellDoubleClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        Try
            Dim w = DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(0).FormattedValue().ToString()
            par2 = w
            par_cnt = "CLIENTEFORNITORE"


        Catch ex As Exception

        End Try

        Form2.Show()
    End Sub

    Private Sub Button6_Click(sender As Object, e As EventArgs) Handles Button6.Click
        connessione()

        Try
            Dim s As String
            If (ComboBox2.SelectedItem.ToString().Equals("Offerta")) Then
                s = "id_offerta"
            ElseIf (ComboBox2.SelectedItem.ToString().Equals("Cliente")) Then
                s = "clientefornitore"
            ElseIf (ComboBox2.SelectedItem.ToString().Equals("Data")) Then
                s = "data_off"

            End If


            dbcomm = New MySqlCommand("select * from OFFERTA where " + s + " = '" + TextBox6.Text + "'", cn)
            dbread = dbcomm.ExecuteReader()

            DataGridView3.Rows.Clear()

            While dbread.Read()
                DataGridView3.Rows.Add(dbread(0), dbread(1), dbread(2))
            End While
            dbread.Close()
        Catch ex As Exception
            MsgBox("ERRORE")
        End Try
        cn.Close()
    End Sub
End Class
