﻿Imports MySql.Data.MySqlClient

Public Class Form3
    Dim cn As New MySqlConnection
    Dim dbcomm As MySqlCommand
    Dim dbread As MySqlDataReader
    Dim sql As String
    Public riferente As Integer
    Public parametro As String
    Public idofferta As String

    Public Sub connessione()

        Dim connectionString = Form1.connectionString
        Dim connectionString2 = Form1.connectionString2

        cn = New MySqlConnection(connectionString)
        Try
            cn.Open()
        Catch ex As Exception
            MsgBox(sql + "Errore Di Connessione")

        End Try


    End Sub


    Public Sub query(sql As String)
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
        Catch ex As Exception
            MsgBox(sql + "Esiste Già Un Elemento Con Lo Stesso Identificatore")
        End Try

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Form15.Show()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        If Form1.par3.Equals("aggiungi") Then

            connessione()
            query("INSERT INTO OFFERTA(clientefornitore,data_off) VALUES ('" + ComboBox2.SelectedItem.ToString() + "','" + DateTimePicker1.Value.ToString("yyyy-MM-dd") + "');SELECT LAST_INSERT_ID();")
            While dbread.Read()
                idofferta = dbread(0)
            End While

            dbread.Close()
            cn.Close()

            Dim i = 0

            Dim pezzo As String
            Dim revisione As String
            Dim qnt As String
            Dim prezzo As String
            Dim sconto As String
            Dim richiesta As String
            Dim soffiaggio As String
            Dim attr As String
            Dim costoatt As String


            While i < DataGridView2.RowCount
                pezzo = DataGridView2.Rows(i).Cells(0).FormattedValue.ToString()
                revisione = DataGridView2.Rows(i).Cells(1).FormattedValue.ToString()
                qnt = DataGridView2.Rows(i).Cells(2).FormattedValue.ToString()
                prezzo = DataGridView2.Rows(i).Cells(3).FormattedValue.ToString()
                sconto = DataGridView2.Rows(i).Cells(4).FormattedValue.ToString()
                richiesta = DataGridView2.Rows(i).Cells(5).FormattedValue.ToString()
                soffiaggio = DataGridView2.Rows(i).Cells(6).FormattedValue.ToString()
                attr = DataGridView2.Rows(i).Cells(7).FormattedValue.ToString()
                costoatt = DataGridView2.Rows(i).Cells(8).FormattedValue.ToString()


                connessione()
                query("INSERT INTO LISTINO (codice_p,revisione_p,offerta,quantità,sconto_list,prezzo_p,richiesta,soffiaggio,attrezzatura,costo_attrezz) VALUES ('" + pezzo + "','" + revisione + "','" + idofferta + "','" + qnt + "','" + sconto + "','" + prezzo + "','" + richiesta + "','" + soffiaggio + "','" + attr + "','" + costoatt + "')")
                i = i + 1
            End While
        Else

            connessione()
            query("delete from LISTINO where offerta ='" + Form1.idofferta.ToString() + "'")
            dbread.Close()
            cn.Close()




            connessione()
            query("UPDATE OFFERTA SET clientefornitore ='" + ComboBox2.SelectedItem.ToString() + "', data_off='" + DateTimePicker1.Value.ToString("yyyy-MM-dd") + "' where id_offerta='" + Form1.idofferta + "'")


            dbread.Close()
            cn.Close()

            Dim i = 0

            Dim pezzo As String
            Dim revisione As String
            Dim qnt As String
            Dim prezzo As String
            Dim sconto As String
            Dim richiesta As String
            Dim soffiaggio As String
            Dim attr As String
            Dim costoatt As String


            While i < DataGridView2.RowCount
                pezzo = DataGridView2.Rows(i).Cells(0).FormattedValue.ToString()
                revisione = DataGridView2.Rows(i).Cells(1).FormattedValue.ToString()
                qnt = DataGridView2.Rows(i).Cells(2).FormattedValue.ToString()
                prezzo = DataGridView2.Rows(i).Cells(3).FormattedValue.ToString()
                sconto = DataGridView2.Rows(i).Cells(4).FormattedValue.ToString()
                richiesta = DataGridView2.Rows(i).Cells(5).FormattedValue.ToString()
                soffiaggio = DataGridView2.Rows(i).Cells(6).FormattedValue.ToString()
                attr = DataGridView2.Rows(i).Cells(7).FormattedValue.ToString()
                costoatt = DataGridView2.Rows(i).Cells(8).FormattedValue.ToString()
                connessione()

                query("INSERT INTO LISTINO (codice_p,revisione_p,offerta,quantità,sconto_list,prezzo_p,richiesta,soffiaggio,attrezzatura,costo_attrezz) VALUES ('" + pezzo + "','" + revisione + "','" + Form1.idofferta + "','" + qnt + "','" + sconto + "','" + prezzo + "','" + richiesta + "','" + soffiaggio + "','" + attr + "','" + costoatt + "')")
                i = i + 1
            End While

        End If

        Form1.loadOfferte()
        Form1.loadListino()
        Me.Close()
    End Sub

    Private Sub Form3_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        connessione()
        query("select * from clientefornitore where cliente = true")
        While dbread.Read()
            ComboBox2.Items.Add(dbread(0))
        End While

        If Form1.par3.Equals("modifica") Or Form1.par3.Equals("visualizza") Then
            Button2.Text = "Modifica Offerta"
            connessione()
            query("select * from OFFERTA where id_offerta=" + Form1.idofferta)
            While dbread.Read()
                ComboBox2.Text = dbread(1)
            End While
            dbread.Close()
            cn.Close()

            connessione()
            query("select * from LISTINO where offerta = " + Form1.idofferta)
            While dbread.Read
                DataGridView2.Rows.Add(dbread(0), dbread(1), dbread(3), dbread(4), dbread(5), dbread(6), dbread(7), dbread(8), dbread(9))
            End While
        End If
        If Form1.par3.Equals("visualizza") Then
            Button1.Hide()
            Button2.Hide()
            Elimina.Hide()
            ComboBox2.Enabled = False
            DateTimePicker1.Enabled = False
            DataGridView2.Enabled = False

        End If
    End Sub

    Private Sub Elimina_Click(sender As Object, e As EventArgs) Handles Elimina.Click
        DataGridView2.Rows.Remove(DataGridView2.CurrentRow)
    End Sub


End Class