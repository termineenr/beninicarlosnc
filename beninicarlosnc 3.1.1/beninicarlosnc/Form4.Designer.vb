﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form4
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.DateTimePicker3 = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.DateTimePicker2 = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.TextBox8 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.DateTimePicker1 = New System.Windows.Forms.DateTimePicker()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.ComboBox2 = New System.Windows.Forms.ComboBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.pezzo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.revisione = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.quantità = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.prezzo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lotto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.attrezzatura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.costo_attr = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.soffiaggio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(450, 20)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(164, 20)
        Me.TextBox2.TabIndex = 85
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(335, 23)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(109, 13)
        Me.Label9.TabIndex = 84
        Me.Label9.Text = "Codice Ordine Cliente"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(432, 61)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(13, 13)
        Me.Label6.TabIndex = 83
        Me.Label6.Text = "€"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(226, 60)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(13, 13)
        Me.Label5.TabIndex = 82
        Me.Label5.Text = "€"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(857, 283)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(79, 24)
        Me.Button3.TabIndex = 81
        Me.Button3.Text = "Elimina"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(772, 283)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(79, 24)
        Me.Button2.TabIndex = 80
        Me.Button2.Text = "Aggiungi"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 110)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 13)
        Me.Label4.TabIndex = 79
        Me.Label4.Text = "Descrizione"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(120, 57)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 20)
        Me.TextBox1.TabIndex = 78
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 60)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 77
        Me.Label1.Text = "Prezzo totale"
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.pezzo, Me.revisione, Me.quantità, Me.prezzo, Me.lotto, Me.attrezzatura, Me.costo_attr, Me.soffiaggio})
        Me.DataGridView1.Location = New System.Drawing.Point(26, 163)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.Size = New System.Drawing.Size(910, 114)
        Me.DataGridView1.TabIndex = 76
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(50, 323)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(130, 26)
        Me.Button1.TabIndex = 75
        Me.Button1.Text = "Aggiungi Ordine"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'DateTimePicker3
        '
        Me.DateTimePicker3.Location = New System.Drawing.Point(736, 118)
        Me.DateTimePicker3.Name = "DateTimePicker3"
        Me.DateTimePicker3.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker3.TabIndex = 74
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(639, 118)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(80, 13)
        Me.Label3.TabIndex = 73
        Me.Label3.Text = "Data consegna"
        '
        'DateTimePicker2
        '
        Me.DateTimePicker2.Location = New System.Drawing.Point(736, 65)
        Me.DateTimePicker2.Name = "DateTimePicker2"
        Me.DateTimePicker2.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker2.TabIndex = 72
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(639, 65)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(91, 13)
        Me.Label2.TabIndex = 71
        Me.Label2.Text = "Data distribuzione"
        '
        'TextBox8
        '
        Me.TextBox8.Location = New System.Drawing.Point(326, 58)
        Me.TextBox8.Name = "TextBox8"
        Me.TextBox8.Size = New System.Drawing.Size(100, 20)
        Me.TextBox8.TabIndex = 70
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(279, 60)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(41, 13)
        Me.Label11.TabIndex = 69
        Me.Label11.Text = "Sconto"
        '
        'TextBox6
        '
        Me.TextBox6.Location = New System.Drawing.Point(120, 95)
        Me.TextBox6.Multiline = True
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(494, 44)
        Me.TextBox6.TabIndex = 68
        '
        'DateTimePicker1
        '
        Me.DateTimePicker1.Location = New System.Drawing.Point(736, 17)
        Me.DateTimePicker1.Name = "DateTimePicker1"
        Me.DateTimePicker1.Size = New System.Drawing.Size(200, 20)
        Me.DateTimePicker1.TabIndex = 67
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(639, 17)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(62, 13)
        Me.Label8.TabIndex = 66
        Me.Label8.Text = "Data ordine"
        '
        'ComboBox2
        '
        Me.ComboBox2.FormattingEnabled = True
        Me.ComboBox2.Location = New System.Drawing.Point(120, 20)
        Me.ComboBox2.Name = "ComboBox2"
        Me.ComboBox2.Size = New System.Drawing.Size(200, 21)
        Me.ComboBox2.TabIndex = 65
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(23, 23)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(39, 13)
        Me.Label7.TabIndex = 64
        Me.Label7.Text = "Cliente"
        '
        'pezzo
        '
        Me.pezzo.HeaderText = "Pezzo"
        Me.pezzo.Name = "pezzo"
        Me.pezzo.ReadOnly = True
        Me.pezzo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.pezzo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'revisione
        '
        Me.revisione.HeaderText = "Revisione"
        Me.revisione.Name = "revisione"
        Me.revisione.ReadOnly = True
        Me.revisione.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.revisione.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'quantità
        '
        Me.quantità.FillWeight = 75.0!
        Me.quantità.HeaderText = "Quantita "
        Me.quantità.Name = "quantità"
        Me.quantità.ReadOnly = True
        Me.quantità.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.quantità.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'prezzo
        '
        Me.prezzo.FillWeight = 75.0!
        Me.prezzo.HeaderText = "Prezzo"
        Me.prezzo.Name = "prezzo"
        Me.prezzo.ReadOnly = True
        Me.prezzo.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.prezzo.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable
        '
        'lotto
        '
        Me.lotto.HeaderText = "Lotto"
        Me.lotto.Name = "lotto"
        '
        'attrezzatura
        '
        Me.attrezzatura.HeaderText = "Attrezzatura"
        Me.attrezzatura.Name = "attrezzatura"
        Me.attrezzatura.ReadOnly = True
        '
        'costo_attr
        '
        Me.costo_attr.HeaderText = "Costo Attrezzaggio"
        Me.costo_attr.Name = "costo_attr"
        Me.costo_attr.ReadOnly = True
        '
        'soffiaggio
        '
        Me.soffiaggio.HeaderText = "Soffiaggio"
        Me.soffiaggio.Name = "soffiaggio"
        Me.soffiaggio.ReadOnly = True
        '
        'Form4
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(948, 374)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.DateTimePicker3)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.DateTimePicker2)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.TextBox8)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.TextBox6)
        Me.Controls.Add(Me.DateTimePicker1)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.ComboBox2)
        Me.Controls.Add(Me.Label7)
        Me.MaximizeBox = False
        Me.Name = "Form4"
        Me.Text = "Ordine - BeniniCarloSNC"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents Button1 As Button
    Friend WithEvents DateTimePicker3 As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents DateTimePicker2 As DateTimePicker
    Friend WithEvents Label2 As Label
    Friend WithEvents TextBox8 As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents TextBox6 As TextBox
    Friend WithEvents DateTimePicker1 As DateTimePicker
    Friend WithEvents Label8 As Label
    Friend WithEvents ComboBox2 As ComboBox
    Friend WithEvents Label7 As Label
    Friend WithEvents pezzo As DataGridViewTextBoxColumn
    Friend WithEvents revisione As DataGridViewTextBoxColumn
    Friend WithEvents quantità As DataGridViewTextBoxColumn
    Friend WithEvents prezzo As DataGridViewTextBoxColumn
    Friend WithEvents lotto As DataGridViewTextBoxColumn
    Friend WithEvents attrezzatura As DataGridViewTextBoxColumn
    Friend WithEvents costo_attr As DataGridViewTextBoxColumn
    Friend WithEvents soffiaggio As DataGridViewTextBoxColumn
End Class
