﻿Imports MySql.Data.MySqlClient

Public Class Form10
    Public pezzo As String
    Public revisione As String
    Public materiale As String
    Public maschera As String
    Dim cn As New MySqlConnection
    Dim dbcomm As MySqlCommand
    Dim dbread As MySqlDataReader

    Public Sub connessione()

        Dim connectionString = Form1.connectionString
        Dim connectionString2 = Form1.connectionString2
        cn = New MySqlConnection(connectionString)
        Try
            cn.Open()
        Catch ex As Exception
            MsgBox("Errore Di Connessione")

        End Try


    End Sub


    Public Sub query(sql As String)
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
        Catch ex As Exception
            MsgBox("Esiste Già Un Elemento Con Lo Stesso Identificatore")
        End Try

    End Sub
    Private Sub Form10_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        id_materiale.Text = "Nessun materiale"
        fornitore.Text = ""
        lega.Text = ""
        prezzokg.Text = ""
        dimensioni.Text = ""
        costo_mat.Text = ""


        loadPezzo()
        loadMateriale()
        loadMaschera()

    End Sub
    Public Sub loadPezzo()


        connessione()
        query("select  * from PEZZO where codice_p='" + pezzo + "' and revisione_p='" + revisione + "'")
        Try
            While dbread.Read()
                codice_p.Text = dbread(0)
                codp_cliente.Text = dbread(1)
                desc_p.Text = dbread(2)
                materiale = dbread(3)
                tipo.Text = dbread(4)
                maschera = dbread(5)
                lavorazione.Text = dbread(6)
                programma.Text = dbread(7)
                revisione_p.Text = dbread(8)
            End While
        Catch ex As Exception
            MsgBox("ERRORE")
        End Try
        dbread.Close()
        cn.Close()
    End Sub
    Public Sub loadMaschera()


        connessione()
        query("select  * from MASCHERA where codice_mas='" + maschera + "'")
        Try
            While dbread.Read()
                codice_mas.Text = dbread(0)
                materiale_mas.Text = dbread(1)
                tempi_attrezz.Text = dbread(2)
            End While
        Catch ex As Exception
            MsgBox("ERRORE")
        End Try
        dbread.Close()
        cn.Close()
    End Sub
    Public Sub loadMateriale()

        If tipo.Text = "Pieno" Then


            connessione()
            query("select distinct * from MATERIALE where id_materiale='" + materiale + "'")
            Try
                While dbread.Read()
                    id_materiale.Text = dbread(0)
                    fornitore.Text = dbread(1)
                    lega.Text = dbread(2)
                    prezzokg.Text = dbread(3)
                    dimensioni.Text = dbread(4)
                    costo_mat.Text = dbread(5)
                End While
            Catch ex As Exception
                MsgBox("ERRORE")
            End Try
            dbread.Close()
            cn.Close()
        Else


            fornitore.Text = ""
            lega.Text = ""
            prezzokg.Text = ""
            dimensioni.Text = ""
            costo_mat.Text = ""
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()

    End Sub
End Class