﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form8
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.prezzokg = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.lega = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.costo_mat = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dimensioni = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.fornitore = New System.Windows.Forms.ComboBox()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(132, 237)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 5
        Me.Button1.Text = "Aggiungi"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'prezzokg
        '
        Me.prezzokg.Location = New System.Drawing.Point(112, 66)
        Me.prezzokg.Name = "prezzokg"
        Me.prezzokg.Size = New System.Drawing.Size(240, 20)
        Me.prezzokg.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(16, 69)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(65, 13)
        Me.Label3.TabIndex = 90
        Me.Label3.Text = "Prezzo al kg"
        '
        'lega
        '
        Me.lega.Location = New System.Drawing.Point(112, 27)
        Me.lega.Name = "lega"
        Me.lega.Size = New System.Drawing.Size(241, 20)
        Me.lega.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 34)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(31, 13)
        Me.Label1.TabIndex = 87
        Me.Label1.Text = "Lega"
        '
        'costo_mat
        '
        Me.costo_mat.Location = New System.Drawing.Point(112, 143)
        Me.costo_mat.Name = "costo_mat"
        Me.costo_mat.Size = New System.Drawing.Size(240, 20)
        Me.costo_mat.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(16, 146)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(79, 13)
        Me.Label2.TabIndex = 95
        Me.Label2.Text = "Costo materiale"
        '
        'dimensioni
        '
        Me.dimensioni.Location = New System.Drawing.Point(112, 104)
        Me.dimensioni.Name = "dimensioni"
        Me.dimensioni.Size = New System.Drawing.Size(241, 20)
        Me.dimensioni.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(16, 111)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(58, 13)
        Me.Label4.TabIndex = 93
        Me.Label4.Text = "Dimensioni"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(16, 187)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(48, 13)
        Me.Label5.TabIndex = 97
        Me.Label5.Text = "Fornitore"
        '
        'fornitore
        '
        Me.fornitore.FormattingEnabled = True
        Me.fornitore.Location = New System.Drawing.Point(112, 184)
        Me.fornitore.Name = "fornitore"
        Me.fornitore.Size = New System.Drawing.Size(240, 21)
        Me.fornitore.TabIndex = 4
        '
        'Form8
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(376, 295)
        Me.Controls.Add(Me.fornitore)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.costo_mat)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dimensioni)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.prezzokg)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.lega)
        Me.Controls.Add(Me.Label1)
        Me.MaximizeBox = False
        Me.Name = "Form8"
        Me.Text = "Materiale - Benini Carlo SNC"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents prezzokg As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents lega As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents costo_mat As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents dimensioni As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents fornitore As ComboBox
End Class
