﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form7
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.codice_mas = New System.Windows.Forms.TextBox()
        Me.materiale_mas = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.tempi_attrezz = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'codice_mas
        '
        Me.codice_mas.Location = New System.Drawing.Point(117, 12)
        Me.codice_mas.Name = "codice_mas"
        Me.codice_mas.Size = New System.Drawing.Size(240, 20)
        Me.codice_mas.TabIndex = 0
        '
        'materiale_mas
        '
        Me.materiale_mas.Location = New System.Drawing.Point(116, 51)
        Me.materiale_mas.Name = "materiale_mas"
        Me.materiale_mas.Size = New System.Drawing.Size(241, 20)
        Me.materiale_mas.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(20, 58)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(50, 13)
        Me.Label1.TabIndex = 78
        Me.Label1.Text = "Materiale"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(20, 14)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 77
        Me.Label7.Text = "Codice "
        '
        'tempi_attrezz
        '
        Me.tempi_attrezz.Location = New System.Drawing.Point(116, 90)
        Me.tempi_attrezz.Name = "tempi_attrezz"
        Me.tempi_attrezz.Size = New System.Drawing.Size(240, 20)
        Me.tempi_attrezz.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(20, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(96, 13)
        Me.Label3.TabIndex = 81
        Me.Label3.Text = "Tempi attrezzaggio"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(141, 147)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Aggiungi"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Form7
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(387, 205)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.tempi_attrezz)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.codice_mas)
        Me.Controls.Add(Me.materiale_mas)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label7)
        Me.MaximizeBox = False
        Me.Name = "Form7"
        Me.Text = "Maschera - Benini Carlo SNC"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents codice_mas As TextBox
    Friend WithEvents materiale_mas As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents tempi_attrezz As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Button1 As Button
End Class
