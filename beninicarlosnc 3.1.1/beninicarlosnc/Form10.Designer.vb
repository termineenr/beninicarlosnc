﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form10
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.codice_p = New System.Windows.Forms.Label()
        Me.codp_cliente = New System.Windows.Forms.Label()
        Me.revisione_p = New System.Windows.Forms.Label()
        Me.programma = New System.Windows.Forms.Label()
        Me.desc_p = New System.Windows.Forms.Label()
        Me.lavorazione = New System.Windows.Forms.Label()
        Me.tipo = New System.Windows.Forms.Label()
        Me.codice_mas = New System.Windows.Forms.Label()
        Me.materiale_mas = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.tempi_attrezz = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.id_materiale = New System.Windows.Forms.Label()
        Me.fornitore = New System.Windows.Forms.Label()
        Me.lega = New System.Windows.Forms.Label()
        Me.prezzokg = New System.Windows.Forms.Label()
        Me.dimensioni = New System.Windows.Forms.Label()
        Me.costo_mat = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(12, 189)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 13)
        Me.Label6.TabIndex = 137
        Me.Label6.Text = "Programma"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(546, 69)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(82, 13)
        Me.Label5.TabIndex = 136
        Me.Label5.Text = "Materiale Pezzo"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(12, 150)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 135
        Me.Label3.Text = "Revisione"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(12, 274)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 13)
        Me.Label4.TabIndex = 134
        Me.Label4.Text = "Lavorazione"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(315, 69)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 133
        Me.Label2.Text = "Maschera"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 107)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 13)
        Me.Label1.TabIndex = 132
        Me.Label1.Text = "Codice Cliente"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(12, 231)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(62, 13)
        Me.Label11.TabIndex = 131
        Me.Label11.Text = "Descrizione"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(12, 69)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 130
        Me.Label7.Text = "Codice "
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(12, 313)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(28, 13)
        Me.Label8.TabIndex = 138
        Me.Label8.Text = "Tipo"
        '
        'codice_p
        '
        Me.codice_p.AutoSize = True
        Me.codice_p.Location = New System.Drawing.Point(171, 71)
        Me.codice_p.Name = "codice_p"
        Me.codice_p.Size = New System.Drawing.Size(39, 13)
        Me.codice_p.TabIndex = 139
        Me.codice_p.Text = "Label9"
        '
        'codp_cliente
        '
        Me.codp_cliente.AutoSize = True
        Me.codp_cliente.Location = New System.Drawing.Point(171, 107)
        Me.codp_cliente.Name = "codp_cliente"
        Me.codp_cliente.Size = New System.Drawing.Size(39, 13)
        Me.codp_cliente.TabIndex = 140
        Me.codp_cliente.Text = "Label9"
        '
        'revisione_p
        '
        Me.revisione_p.AutoSize = True
        Me.revisione_p.Location = New System.Drawing.Point(171, 150)
        Me.revisione_p.Name = "revisione_p"
        Me.revisione_p.Size = New System.Drawing.Size(39, 13)
        Me.revisione_p.TabIndex = 141
        Me.revisione_p.Text = "Label9"
        '
        'programma
        '
        Me.programma.AutoSize = True
        Me.programma.Location = New System.Drawing.Point(171, 189)
        Me.programma.Name = "programma"
        Me.programma.Size = New System.Drawing.Size(39, 13)
        Me.programma.TabIndex = 142
        Me.programma.Text = "Label9"
        '
        'desc_p
        '
        Me.desc_p.AutoSize = True
        Me.desc_p.Location = New System.Drawing.Point(171, 231)
        Me.desc_p.Name = "desc_p"
        Me.desc_p.Size = New System.Drawing.Size(39, 13)
        Me.desc_p.TabIndex = 143
        Me.desc_p.Text = "Label9"
        '
        'lavorazione
        '
        Me.lavorazione.AutoSize = True
        Me.lavorazione.Location = New System.Drawing.Point(171, 274)
        Me.lavorazione.Name = "lavorazione"
        Me.lavorazione.Size = New System.Drawing.Size(39, 13)
        Me.lavorazione.TabIndex = 144
        Me.lavorazione.Text = "Label9"
        '
        'tipo
        '
        Me.tipo.AutoSize = True
        Me.tipo.Location = New System.Drawing.Point(171, 313)
        Me.tipo.Name = "tipo"
        Me.tipo.Size = New System.Drawing.Size(39, 13)
        Me.tipo.TabIndex = 145
        Me.tipo.Text = "Label9"
        '
        'codice_mas
        '
        Me.codice_mas.AutoSize = True
        Me.codice_mas.Location = New System.Drawing.Point(421, 71)
        Me.codice_mas.Name = "codice_mas"
        Me.codice_mas.Size = New System.Drawing.Size(39, 13)
        Me.codice_mas.TabIndex = 146
        Me.codice_mas.Text = "Label9"
        '
        'materiale_mas
        '
        Me.materiale_mas.AutoSize = True
        Me.materiale_mas.Location = New System.Drawing.Point(421, 107)
        Me.materiale_mas.Name = "materiale_mas"
        Me.materiale_mas.Size = New System.Drawing.Size(39, 13)
        Me.materiale_mas.TabIndex = 147
        Me.materiale_mas.Text = "Label9"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(315, 107)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(100, 13)
        Me.Label9.TabIndex = 148
        Me.Label9.Text = "Materiale Maschera"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(315, 150)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(97, 13)
        Me.Label10.TabIndex = 149
        Me.Label10.Text = "Tempi Attrezzaggio"
        '
        'tempi_attrezz
        '
        Me.tempi_attrezz.AutoSize = True
        Me.tempi_attrezz.Location = New System.Drawing.Point(421, 150)
        Me.tempi_attrezz.Name = "tempi_attrezz"
        Me.tempi_attrezz.Size = New System.Drawing.Size(39, 13)
        Me.tempi_attrezz.TabIndex = 150
        Me.tempi_attrezz.Text = "Label9"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(546, 107)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(48, 13)
        Me.Label12.TabIndex = 151
        Me.Label12.Text = "Fornitore"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(546, 150)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(31, 13)
        Me.Label13.TabIndex = 152
        Me.Label13.Text = "Lega"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(546, 189)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(54, 13)
        Me.Label14.TabIndex = 153
        Me.Label14.Text = "Prezzo kg"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(546, 231)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(58, 13)
        Me.Label15.TabIndex = 154
        Me.Label15.Text = "Dimensioni"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(546, 274)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(37, 13)
        Me.Label16.TabIndex = 155
        Me.Label16.Text = "Costo "
        '
        'id_materiale
        '
        Me.id_materiale.AutoSize = True
        Me.id_materiale.Location = New System.Drawing.Point(661, 69)
        Me.id_materiale.Name = "id_materiale"
        Me.id_materiale.Size = New System.Drawing.Size(45, 13)
        Me.id_materiale.TabIndex = 156
        Me.id_materiale.Text = "Label17"
        '
        'fornitore
        '
        Me.fornitore.AutoSize = True
        Me.fornitore.Location = New System.Drawing.Point(661, 107)
        Me.fornitore.Name = "fornitore"
        Me.fornitore.Size = New System.Drawing.Size(45, 13)
        Me.fornitore.TabIndex = 157
        Me.fornitore.Text = "Label17"
        '
        'lega
        '
        Me.lega.AutoSize = True
        Me.lega.Location = New System.Drawing.Point(661, 150)
        Me.lega.Name = "lega"
        Me.lega.Size = New System.Drawing.Size(45, 13)
        Me.lega.TabIndex = 158
        Me.lega.Text = "Label17"
        '
        'prezzokg
        '
        Me.prezzokg.AutoSize = True
        Me.prezzokg.Location = New System.Drawing.Point(661, 189)
        Me.prezzokg.Name = "prezzokg"
        Me.prezzokg.Size = New System.Drawing.Size(45, 13)
        Me.prezzokg.TabIndex = 159
        Me.prezzokg.Text = "Label17"
        '
        'dimensioni
        '
        Me.dimensioni.AutoSize = True
        Me.dimensioni.Location = New System.Drawing.Point(661, 231)
        Me.dimensioni.Name = "dimensioni"
        Me.dimensioni.Size = New System.Drawing.Size(45, 13)
        Me.dimensioni.TabIndex = 160
        Me.dimensioni.Text = "Label17"
        '
        'costo_mat
        '
        Me.costo_mat.AutoSize = True
        Me.costo_mat.Location = New System.Drawing.Point(661, 274)
        Me.costo_mat.Name = "costo_mat"
        Me.costo_mat.Size = New System.Drawing.Size(45, 13)
        Me.costo_mat.TabIndex = 161
        Me.costo_mat.Text = "Label17"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(343, 302)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 162
        Me.Button1.Text = "Chiudi"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label17.Location = New System.Drawing.Point(276, 20)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(228, 25)
        Me.Label17.TabIndex = 163
        Me.Label17.Text = "Visualizzazione Pezzo"
        '
        'Form10
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(786, 380)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.costo_mat)
        Me.Controls.Add(Me.dimensioni)
        Me.Controls.Add(Me.prezzokg)
        Me.Controls.Add(Me.lega)
        Me.Controls.Add(Me.fornitore)
        Me.Controls.Add(Me.id_materiale)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.tempi_attrezz)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.materiale_mas)
        Me.Controls.Add(Me.codice_mas)
        Me.Controls.Add(Me.tipo)
        Me.Controls.Add(Me.lavorazione)
        Me.Controls.Add(Me.desc_p)
        Me.Controls.Add(Me.programma)
        Me.Controls.Add(Me.revisione_p)
        Me.Controls.Add(Me.codp_cliente)
        Me.Controls.Add(Me.codice_p)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label7)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Name = "Form10"
        Me.Text = "Pezzo - Benini Carlo SNC"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents codice_p As Label
    Friend WithEvents codp_cliente As Label
    Friend WithEvents revisione_p As Label
    Friend WithEvents programma As Label
    Friend WithEvents desc_p As Label
    Friend WithEvents lavorazione As Label
    Friend WithEvents tipo As Label
    Friend WithEvents codice_mas As Label
    Friend WithEvents materiale_mas As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents tempi_attrezz As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents id_materiale As Label
    Friend WithEvents fornitore As Label
    Friend WithEvents lega As Label
    Friend WithEvents prezzokg As Label
    Friend WithEvents dimensioni As Label
    Friend WithEvents costo_mat As Label
    Friend WithEvents Button1 As Button
    Friend WithEvents Label17 As Label
End Class
