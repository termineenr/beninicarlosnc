﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form5
    Inherits System.Windows.Forms.Form

    'Form esegue l'override del metodo Dispose per pulire l'elenco dei componenti.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Richiesto da Progettazione Windows Form
    Private components As System.ComponentModel.IContainer

    'NOTA: la procedura che segue è richiesta da Progettazione Windows Form
    'Può essere modificata in Progettazione Windows Form.  
    'Non modificarla mediante l'editor del codice.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.codice = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.desc_p = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.codice_cli = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.maschera = New System.Windows.Forms.ComboBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lavorazione = New System.Windows.Forms.TextBox()
        Me.revisione_p = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.materiale = New System.Windows.Forms.ComboBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.programma = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.RadioButton1 = New System.Windows.Forms.RadioButton()
        Me.RadioButton2 = New System.Windows.Forms.RadioButton()
        Me.SuspendLayout()
        '
        'codice
        '
        Me.codice.Location = New System.Drawing.Point(98, 25)
        Me.codice.Name = "codice"
        Me.codice.Size = New System.Drawing.Size(103, 20)
        Me.codice.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 68)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 13)
        Me.Label1.TabIndex = 74
        Me.Label1.Text = "Codice Cliente"
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(670, 17)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(111, 21)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Aggiungi maschera"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(19, 256)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(130, 26)
        Me.Button1.TabIndex = 12
        Me.Button1.Text = "Aggiungi"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(16, 125)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(62, 13)
        Me.Label11.TabIndex = 65
        Me.Label11.Text = "Descrizione"
        '
        'desc_p
        '
        Me.desc_p.Location = New System.Drawing.Point(98, 108)
        Me.desc_p.Multiline = True
        Me.desc_p.Name = "desc_p"
        Me.desc_p.Size = New System.Drawing.Size(295, 105)
        Me.desc_p.TabIndex = 8
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(16, 24)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(43, 13)
        Me.Label7.TabIndex = 57
        Me.Label7.Text = "Codice "
        '
        'codice_cli
        '
        Me.codice_cli.Location = New System.Drawing.Point(98, 62)
        Me.codice_cli.Name = "codice_cli"
        Me.codice_cli.Size = New System.Drawing.Size(103, 20)
        Me.codice_cli.TabIndex = 4
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(429, 21)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(54, 13)
        Me.Label2.TabIndex = 77
        Me.Label2.Text = "Maschera"
        '
        'maschera
        '
        Me.maschera.FormattingEnabled = True
        Me.maschera.Location = New System.Drawing.Point(503, 17)
        Me.maschera.Name = "maschera"
        Me.maschera.Size = New System.Drawing.Size(149, 21)
        Me.maschera.TabIndex = 2
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(429, 125)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 13)
        Me.Label4.TabIndex = 111
        Me.Label4.Text = "Lavorazione"
        '
        'lavorazione
        '
        Me.lavorazione.Location = New System.Drawing.Point(503, 108)
        Me.lavorazione.Multiline = True
        Me.lavorazione.Name = "lavorazione"
        Me.lavorazione.Size = New System.Drawing.Size(278, 105)
        Me.lavorazione.TabIndex = 9
        '
        'revisione_p
        '
        Me.revisione_p.Location = New System.Drawing.Point(292, 21)
        Me.revisione_p.Name = "revisione_p"
        Me.revisione_p.Size = New System.Drawing.Size(101, 20)
        Me.revisione_p.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(226, 25)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(54, 13)
        Me.Label3.TabIndex = 112
        Me.Label3.Text = "Revisione"
        '
        'materiale
        '
        Me.materiale.FormattingEnabled = True
        Me.materiale.Location = New System.Drawing.Point(503, 61)
        Me.materiale.Name = "materiale"
        Me.materiale.Size = New System.Drawing.Size(149, 21)
        Me.materiale.TabIndex = 6
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(429, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(50, 13)
        Me.Label5.TabIndex = 115
        Me.Label5.Text = "Materiale"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(670, 61)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(111, 21)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "Aggiungi Materiale"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'programma
        '
        Me.programma.Location = New System.Drawing.Point(292, 61)
        Me.programma.Name = "programma"
        Me.programma.Size = New System.Drawing.Size(101, 20)
        Me.programma.TabIndex = 5
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(226, 64)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(60, 13)
        Me.Label6.TabIndex = 117
        Me.Label6.Text = "Programma"
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Location = New System.Drawing.Point(98, 219)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(52, 17)
        Me.RadioButton1.TabIndex = 10
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "Pieno"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(345, 219)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(48, 17)
        Me.RadioButton2.TabIndex = 11
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "Fuso"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'Form5
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(828, 309)
        Me.Controls.Add(Me.RadioButton2)
        Me.Controls.Add(Me.RadioButton1)
        Me.Controls.Add(Me.programma)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.materiale)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.revisione_p)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lavorazione)
        Me.Controls.Add(Me.maschera)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.codice_cli)
        Me.Controls.Add(Me.codice)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.desc_p)
        Me.Controls.Add(Me.Label7)
        Me.MaximizeBox = False
        Me.Name = "Form5"
        Me.Text = "Pezzo - Benini Carlo SNC "
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents codice As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents Label11 As Label
    Friend WithEvents desc_p As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents codice_cli As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents maschera As ComboBox
    Friend WithEvents Label4 As Label
    Friend WithEvents lavorazione As TextBox
    Friend WithEvents revisione_p As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents materiale As ComboBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents programma As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents RadioButton1 As RadioButton
    Friend WithEvents RadioButton2 As RadioButton
End Class
