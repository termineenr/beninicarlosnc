﻿Imports MySql.Data.MySqlClient

Public Class Form4

    Dim cn As New MySqlConnection
    Dim dbcomm As MySqlCommand
    Dim dbread As MySqlDataReader
    Dim ordine As Integer
    Public offerta As Integer

    Public Sub connessione()

        Dim connectionString = Form1.connectionString
        Dim connectionString2 = Form1.connectionString2
        cn = New MySqlConnection(connectionString)
        Try
            cn.Open()
        Catch ex As Exception
            MsgBox("Errore Di Connessione")

        End Try


    End Sub

    Private Sub Form4_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Button1.Text = Form1.AME
        TextBox1.Text = "0"
        TextBox8.Text = "0"
        offerta = 0


        connessione()
        Dim Sql = "Select * from CLIENTEFORNITORE where cliente=true"
        dbcomm = New MySqlCommand(Sql, cn)
        dbread = dbcomm.ExecuteReader()
        DataGridView1.Rows.Clear()
        Dim i = 0
        While dbread.Read()
            ComboBox2.Items.Insert(i, dbread(0))
            i = i + 1
        End While
        dbread.Close()
        cn.Close()


        If Form1.AME = "Modifica" Or Form1.AME = "Visualizza" Then
            ordine = Form1.par6

            Sql = "SELECT * FROM ORDINE WHERE id_ordine= " + ordine.ToString()
            Try
                connessione()
                dbcomm = New MySqlCommand(Sql, cn)
                dbread = dbcomm.ExecuteReader()
                While dbread.Read()
                    Dim cli As String = dbread(2)
                    ComboBox2.Text = cli
                    DateTimePicker3.Value = dbread(8)
                    DateTimePicker2.Value = dbread(7)
                    DateTimePicker1.Value = dbread(4)
                    TextBox1.Text = dbread(9)
                    TextBox2.Text = dbread(1)
                    TextBox6.Text = dbread(5)

                    TextBox8.Text = dbread(6)
                End While

                dbread.Close()
                cn.Close()
            Catch ex As Exception
                MsgBox("ERRORE QUI!!!!!")
            End Try

            Sql = "SELECT * FROM ORDINEPEZZO WHERE ordine=" + ordine.ToString()
            Try
                connessione()
                dbcomm = New MySqlCommand(Sql, cn)
                dbread = dbcomm.ExecuteReader()
            Catch ex As Exception
                MsgBox("ERRORE QUA!!!!!")
            End Try

            Try
                i = 0
                While dbread.Read()
                    DataGridView1.Rows.Add(dbread(1), dbread(2), dbread(3), dbread(5), dbread(4), dbread(6), dbread(7), dbread(8))
                End While

                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE QUA!!!!!")
            End Try
            If Form1.AME = "Visualizza" Then
                cn.Close()
                Button1.Hide()
                Button2.Hide()
                Button3.Hide()
                TextBox1.Enabled = False
                TextBox2.Enabled = False
                TextBox6.Enabled = False
                TextBox8.Enabled = False
                DateTimePicker1.Enabled = False
                DateTimePicker2.Enabled = False
                DateTimePicker3.Enabled = False
                ComboBox2.Enabled = False
                DataGridView1.Enabled = False
            End If
        End If


    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If Button1.Text = "Aggiungi" Then
            aggiungi()
        ElseIf Button1.Text = "Modifica"
            modifica()
        End If
    End Sub

    Private Sub aggiungi()

        connessione()
        Dim data3 As String = DateTimePicker3.Value.ToString("yyyy-MM-dd")
        Dim data2 As String = DateTimePicker2.Value.ToString("yyyy-MM-dd")
        Dim data1 As String = DateTimePicker1.Value.ToString("yyyy-MM-dd")
        Dim clifor As String = ComboBox2.SelectedItem.ToString()
        Dim sql As String = "INSERT INTO ORDINE(id_cliente,clientefornitore, data_ordine, desc_ordine, sconto_ordine, data_distr, data_cons, prezzo_tot,offerta) VALUES ('" + TextBox2.Text + "','" + clifor + "','" + data1 + "','" + TextBox6.Text + "','" + TextBox8.Text + "','" + data2 + "','" + data3 + "','" + TextBox1.Text + "'," + offerta.ToString() + ")"

        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
            dbread.Close()
        Catch ex As Exception
            MsgBox("ERRORE!!!!!")
        End Try
        cn.Close()


        connessione()
        sql = "SELECT id_ordine FROM ORDINE WHERE id_cliente='" + TextBox2.Text + "' AND clientefornitore='" + clifor + "' AND data_ordine='" + data1 + "' AND desc_ordine='" + TextBox6.Text + "' AND sconto_ordine='" + TextBox8.Text + "' AND data_distr='" + data2 + "' AND data_cons='" + data3 + "' AND prezzo_tot='" + TextBox1.Text + "'"
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
            While dbread.Read()
                ordine = dbread(0)
            End While

            dbread.Close()
        Catch ex As Exception
            MsgBox("ERRORE!!!!!")
        End Try
        cn.Close()

        connessione()
        Dim i As Integer = 0
        Dim pezzo As String
        Dim revisione As String
        Dim qta As String
        Dim lotto As String
        Dim prezzo As String
        Dim attrezz As String
        Dim costo_a As String
        Dim soff As String
        While (i < DataGridView1.RowCount)
            pezzo = DataGridView1.Rows(i).Cells(0).FormattedValue().ToString()
            revisione = DataGridView1.Rows(i).Cells(1).FormattedValue().ToString()
            qta = DataGridView1.Rows(i).Cells(2).FormattedValue().ToString()
            lotto = DataGridView1.Rows(i).Cells(4).FormattedValue().ToString()
            prezzo = DataGridView1.Rows(i).Cells(3).FormattedValue().ToString()
            attrezz = DataGridView1.Rows(i).Cells(5).FormattedValue().ToString()
            costo_a = DataGridView1.Rows(i).Cells(6).FormattedValue().ToString()
            soff = DataGridView1.Rows(i).Cells(7).FormattedValue().ToString()
            If prezzo = "" Then
                prezzo = "0"
            End If
            If qta = "" Then
                qta = "0"
            End If
            If costo_a = "" Then
                costo_a = "0"
            End If
            sql = "INSERT INTO ORDINEPEZZO(ordine, codice_p, revisione_p, quantità_op, lotto, prezzo_op,attrezzatura,costo_attrezz,soffiaggio) VALUES (" + ordine.ToString() + ",'" + pezzo + "','" + revisione + "'," + qta + ",'" + lotto + "'," + prezzo + ",'" + attrezz + "'," + costo_a + ",'" + soff + "')"

            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE!!!!!")
            End Try
            i = i + 1
        End While

        cn.Close()

        Form1.loadOrdine()


        Me.Close()

    End Sub
    Private Sub modifica()
        Dim sql As String

        connessione()

        Dim data3 As String = DateTimePicker3.Value.ToString("yyyy-MM-dd")
        Dim data2 As String = DateTimePicker2.Value.ToString("yyyy-MM-dd")
        Dim data1 As String = DateTimePicker1.Value.ToString("yyyy-MM-dd")
        Dim clifor As String = ComboBox2.SelectedItem.ToString

        If Form1.conf_modifica = 6 Then
            sql = "update ORDINE set id_cliente='" + TextBox2.Text + "', clientefornitore='" + clifor + "' , data_ordine='" + data1 + "' , desc_ordine='" + TextBox6.Text + "' , sconto_ordine=" + TextBox8.Text + " , data_distr='" + data2 + "' , data_cons='" + data3 + "' , prezzo_tot=" + TextBox1.Text + " WHERE id_ordine=" + ordine.ToString() + ""
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
                cn.Close()

            Catch ex As Exception
                MsgBox("Errore modifica")
            End Try

            Dim i As Integer = 0
            Dim pezzo As String
            Dim revisione As String
            Dim qta As String
            Dim lotto As String
            Dim prezzo As String
            Dim attrezz As String
            Dim costo_a As String
            Dim soff As String

            connessione()
            sql = "delete from ORDINEPEZZO where ordine='" + ordine.ToString() + "'"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE !!!!!")
            End Try
            Try
                While (i < DataGridView1.RowCount)

                    pezzo = DataGridView1.Rows(i).Cells(0).FormattedValue().ToString()
                    revisione = DataGridView1.Rows(i).Cells(1).FormattedValue().ToString()
                    qta = DataGridView1.Rows(i).Cells(2).FormattedValue().ToString()
                    lotto = DataGridView1.Rows(i).Cells(4).FormattedValue().ToString()
                    prezzo = DataGridView1.Rows(i).Cells(3).FormattedValue().ToString()
                    attrezz = DataGridView1.Rows(i).Cells(5).FormattedValue().ToString()
                    costo_a = DataGridView1.Rows(i).Cells(6).FormattedValue().ToString()
                    soff = DataGridView1.Rows(i).Cells(7).FormattedValue().ToString()
                    If prezzo = "" Then
                        prezzo = "0"
                    End If
                    If qta = "" Then
                        qta = "0"
                    End If
                    If costo_a = "" Then
                        costo_a = "0"
                    End If

                    sql = "INSERT INTO ORDINEPEZZO(ordine, codice_p, revisione_p, quantità_op, lotto, prezzo_op,attrezzatura,costo_attrezz,soffiaggio) VALUES (" + ordine.ToString() + ",'" + pezzo + "','" + revisione + "'," + qta + ",'" + lotto + "'," + prezzo + ",'" + attrezz + "'," + costo_a + ",'" + soff + "')"

                    Try
                        dbcomm = New MySqlCommand(sql, cn)
                        dbread = dbcomm.ExecuteReader()
                        dbread.Close()
                    Catch ex As Exception
                        MsgBox("ERRORE!!!!!")
                    End Try
                    i = i + 1
                End While
            Catch ex As Exception

            End Try
        End If

        cn.Close()


        Form1.loadOrdine()


        Me.Close()


    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Form14.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        DataGridView1.Rows.Remove(DataGridView1.CurrentRow)
    End Sub
End Class
