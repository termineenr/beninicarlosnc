﻿Imports MySql.Data.MySqlClient

Public Class Form15
    Dim cn As New MySqlConnection
    Dim dbcomm As MySqlCommand
    Dim dbread As MySqlDataReader
    Dim sql As String


    Public Sub connessione()

        Dim connectionString = Form1.connectionString
        Dim connectionString2 = Form1.connectionString2

        cn = New MySqlConnection(connectionString)
        Try
            cn.Open()
        Catch ex As Exception
            MsgBox("Errore Di Connessione")

        End Try


    End Sub


    Public Sub query(sql As String)
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
        Catch ex As Exception
            MsgBox("Esiste Già Un Elemento Con Lo Stesso Identificatore")
        End Try

    End Sub
    Private Sub Form15_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        connessione()
        sql = "select codice_p from PEZZO"
        query(sql)

        While dbread.Read()
            ComboBox1.Items.Add(dbread(0))
        End While

        dbread.Close()
        cn.Close()
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        connessione()
        sql = "select revisione_p from PEZZO where codice_p='" + ComboBox1.SelectedItem.ToString() + "'"
        query(sql)
        ComboBox2.Items.Clear()

        While dbread.Read()
            ComboBox2.Items.Add(dbread(0))
        End While

        dbread.Close()
        cn.Close()
    End Sub

    Private Sub Aggiungi(sender As Object, e As EventArgs) Handles Button1.Click
        If TextBox1.Text = "" Or TextBox2.Text = "" Then
            MsgBox("Quantità e Prezzo devono contenere valori non nulli")
        Else
            Form3.DataGridView2.Rows.Add(ComboBox1.SelectedItem.ToString(), ComboBox2.SelectedItem.ToString(), TextBox1.Text, TextBox2.Text, TextBox3.Text, TextBox4.Text, TextBox5.Text, TextBox6.Text, TextBox7.Text)
            ComboBox1.Text = ""
            ComboBox2.Text = ""
            TextBox1.Text = ""
            TextBox2.Text = ""
            TextBox3.Text = ""
            TextBox4.Text = ""
            TextBox5.Text = ""
            TextBox6.Text = ""
            TextBox7.Text = ""

        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Form1.AME = "Aggiungi"
        Form5.Show()
    End Sub
End Class