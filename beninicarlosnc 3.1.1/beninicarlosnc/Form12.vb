﻿Imports MySql.Data.MySqlClient

Public Class Form12
    Dim cn As New MySqlConnection
    Dim dbcomm As MySqlCommand
    Dim dbread As MySqlDataReader
    Public Function conf_elimina() As Integer
        Return MsgBox("Sicuro di voler eliminare l'elemento selezionato?", 4)
    End Function

    Public Function conf_modifica() As Integer
        Return MsgBox("Sicuro di voler modificare l'elemento selezionato?", 4)
    End Function
    Public Sub connessione()

        Dim connectionString = Form1.connectionString
        Dim connectionString2 = Form1.connectionString2

        cn = New MySqlConnection(connectionString)
        Try
            cn.Open()
        Catch ex As Exception
            MsgBox("Errore Di Connessione")

        End Try


    End Sub


    Public Sub query(sql As String)
        Try
            dbcomm = New MySqlCommand(sql, cn)
            dbread = dbcomm.ExecuteReader()
        Catch ex As Exception
            MsgBox("Esiste Già Un Elemento Con Lo Stesso Identificatore")
        End Try

    End Sub

    Private Sub Form12_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Button4.Text = Form1.AME
        Try

            connessione()
            query("select * from UTENSILE")
            While dbread.Read()
                utensile.Items.Add(dbread(0).ToString())
            End While

        Catch ex As Exception

        End Try
        dbread.Close()
        cn.Close()
        If Form1.AME = "Aggiungi" Then
            connessione()
            Try
                query("select * from MACCHINA")
                While dbread.Read()
                    ComboBox4.Items.Add(dbread(0))
                End While

            Catch ex As Exception

            End Try
            dbread.Close()
            cn.Close()




            Try
                connessione()
                query("select * from PEZZO")
                While dbread.Read()
                    ComboBox1.Items.Add(dbread(0))
                End While
            Catch ex As Exception

            End Try


            dbread.Close()
            cn.Close()

        ElseIf Form1.AME = "Modifica"

            ComboBox1.Text = Form1.par12_pez
            ComboBox2.Text = Form1.par12_rev
            ComboBox4.Text = Form1.par12_mac

            connessione()
            Try
                query("select utensile,posizione from SETPEZZO where codice_p = '" + ComboBox1.Text + "' and revisione_p = '" + ComboBox2.Text + "' and macchina = '" + ComboBox4.Text + "'")
                DataGridView1.Rows.Clear()
                Dim uten As DataGridViewComboBoxCell
                Dim i = 0


                While dbread.Read()



                    DataGridView1.Rows.Add(dbread(1))
                    uten = DataGridView1.Rows(i).Cells(1)
                    uten.Value = dbread(0).ToString()

                    i = i + 1
                End While

                dbread.Close()

            Catch ex As Exception

            End Try
            cn.Close()
            ComboBox4.Enabled = False

            ComboBox1.Enabled = False

            ComboBox2.Enabled = False
        End If

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        If Button4.Text = "Modifica" Then
            connessione()

            If conf_modifica() = 6 Then
                Dim sql = "delete from SETPEZZO where codice_p='" + Form1.par12_pez + "' and macchina='" + Form1.par12_mac + "' and revisione_p='" + Form1.par12_rev + "'"
                Try
                    dbcomm = New MySqlCommand(sql, cn)
                    dbread = dbcomm.ExecuteReader()
                    dbread.Close()
                Catch ex As Exception
                    MsgBox("ERRORE!!!!!")
                End Try
                cn.Close()

            End If

        End If
        Try
            Dim i = 0
            While (DataGridView1.Rows(i).Cells(0).FormattedValue().ToString() <> "")
                connessione()
                Dim s As String
                s = "INSERT INTO SETPEZZO(codice_p, revisione_p, utensile, macchina, posizione) VALUES('" + ComboBox1.Text() + "','" + ComboBox2.Text() + "','" + DataGridView1.Rows(i).Cells(1).FormattedValue().ToString() + "','" + ComboBox4.Text() + "'," + DataGridView1.Rows(i).Cells(0).FormattedValue().ToString() + ")"
                query(s)
                dbread.Close()
                cn.Close()
                i = i + 1
            End While
            MsgBox("Set inserito")
            Form1.loadSet()
            Me.Close()
        Catch ex As Exception
            MsgBox("errore")

        End Try



    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        connessione()
        query("select revisione_p from PEZZO where  codice_p ='" + ComboBox1.SelectedItem.ToString() + "'")
        ComboBox2.Items.Clear()

        While dbread.Read()
            ComboBox2.Items.Add(dbread(0))
        End While

        dbread.Close()
        cn.Close()

    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        connessione()

        If conf_elimina() = 6 Then
            Dim sql = "delete from SETPEZZO where codice_p='" + Form1.par12_pez + "' and macchina='" + Form1.par12_mac + "' and revisione_p='" + Form1.par12_rev + "'and posizione='" + DataGridView1.Rows(DataGridView1.CurrentRow.Index()).Cells(0).FormattedValue().ToString() + "'"
            Try
                dbcomm = New MySqlCommand(sql, cn)
                dbread = dbcomm.ExecuteReader()
                dbread.Close()
            Catch ex As Exception
                MsgBox("ERRORE!!!!!")
            End Try
            cn.Close()
            Form1.loadSet()

            DataGridView1.Rows.Clear()
            connessione()
            Try
                query("select utensile,posizione from SETPEZZO where codice_p = '" + ComboBox1.Text.ToString() + "' and revisione_p = '" + ComboBox2.Text.ToString() + "' and macchina = '" + ComboBox4.Text.ToString() + "'")
                DataGridView1.Rows.Clear()
                Dim uten As DataGridViewComboBoxCell
                Dim i = 0

                While dbread.Read()
                    DataGridView1.Rows.Add(dbread(1))
                    uten = DataGridView1.Rows(i).Cells(1)
                    uten.Value = dbread(0)
                    i = i + 1
                End While

                dbread.Close()
            Catch ex As Exception

            End Try
            cn.Close()

        End If

    End Sub
End Class